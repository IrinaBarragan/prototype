import ConvolutionExplained from './convolutionExplained'
import LutHtml from './lutFotHtml'

class DeptWiseConvExplained extends ConvolutionExplained {

    constructor() {
        super()
        this.modalIsDisplay = false
        window.addEventListener('resize', () => {
            this.drawLines()
        })
        this.min = 100
        this.max = -100
        this.layerWeightsList = []
        this.type = 'depthWise'
    }



    displayModal(inputsPreviousLayerOne, inputsPreviousLayerTwo, inputs, filter, mult, modelLayersName) {
        document.querySelector('#deptWiseConv').style.display = 'block'
        const list = document.querySelector('.operationsList')
        this.blockLayersName(modelLayersName, list)

        this.modalIsDisplay = true
        const keyBoardLOne = document.getElementsByClassName('one')
        const keyBoardLTwo = document.getElementsByClassName('two')
        const keyBoardF = document.getElementsByClassName('gridFiveDW')
        const keyBoardT = document.getElementsByClassName('gridThreeDW')
        const keyBoardM = document.getElementsByClassName('gridMultiplicationDW')
        this.gridFiveGenerate(inputsPreviousLayerOne.inputs5, keyBoardLOne, this.type)
        this.gridFiveGenerate(inputsPreviousLayerTwo.inputs5, keyBoardLTwo, this.type)
        this.gridFiveGenerate(inputs.inputs5, keyBoardF, this.type)
        this.gridThreeGenerate(filter, keyBoardT)
        this.gridThreeGenerate(mult, keyBoardM)

        document.querySelector("#hideConvModalDW").addEventListener("click", () => this.hideModal())

    }
    hideModal() {
        document.querySelector('#deptWiseConv').style.display = 'none'
        // document.querySelector('#conv-final-sum-lines').style.display = 'none'
        let els = document.getElementsByClassName('one')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('operationsList')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('two')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridThreeDW')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridFiveDW')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridMultiplicationDW')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }


    async getFiltersForDeptWise(weights, index) {

        const shapeWeights = { x: weights.shape[0], y: weights.shape[1], f: weights.shape[2], g: weights.shape[3] }

        this.layerWeightsList = await tf.tidy(() => {
            return tf.unstack(weights.reshape([shapeWeights.x, shapeWeights.y, shapeWeights.f, shapeWeights.g]), 2)
        })

        const shapeWeightsIndex = { x: this.layerWeightsList[index].shape[0], y: this.layerWeightsList[index].shape[1], f: this.layerWeightsList[index].shape[2], g: this.layerWeightsList[index].shape[3] }
        const filter = await tf.tidy(() => {
            return tf.unstack(this.layerWeightsList[index].reshape([shapeWeightsIndex.x, shapeWeightsIndex.y, shapeWeightsIndex.f]), 2)
        });
        const filterReversed = tf.tidy(() => { return tf.reverse2d(filter[0]).dataSync() })
        filter.forEach(el => el.dispose())

        this.layerWeightsList.forEach(el => el.dispose())

        return filterReversed

    }

    getOneMap(activMaps, index, position) {

        this.maps = tf.tidy(() => { return tf.reverse2d(activMaps[index]) }).dataSync()

        let dataActueleIndex = 0;
        let inputTensor9 = [];
        let inputTensor25 = []
        let yIndex = 0
        let xIndex = 0
        const a = position.layer
        const array = ['5', '22', '40', '49', '67', '76', '85', '94', '102', '111', '129', '138', '147', '154']
        if (array.includes(a)) {
            yIndex = position.row - 1
            xIndex = position.col - 1

        } else {
            yIndex = position.row * 2
            xIndex = position.col * 2
        }
        let indexThree = 0
        for (let i = 0; i < 3; i++) {
            dataActueleIndex = (yIndex * activMaps[index].shape[0]) + (xIndex) + (i * activMaps[index].shape[0])

            for (let j = 0; j < 3; j++) {
                // extremes
                if ((indexThree % 3 < 1 && position.col === 0) || (indexThree % 3 === 2 && position.col === activMaps[index].shape[0] - 1)) {
                    inputTensor9.push(0)


                } else {
                    if (this.maps[dataActueleIndex + j] === undefined) inputTensor9.push(0)
                    else inputTensor9.push(this.maps[dataActueleIndex + j])
                }
                indexThree++
            }

        }

        inputTensor25 = [];
        let indexFive = 0
        for (let i = -1; i < 4; i++) {
            dataActueleIndex = (yIndex * activMaps[index].shape[0]) + (xIndex) + (i * activMaps[index].shape[0])
            for (let j = -1; j < 4; j++) {
                // extremes
                if (((indexFive % 5 < 1 || indexFive % 5 == 1) && position.col === 0) || ((indexFive % 5 == 4 || indexFive % 5 == 3) && position.col === activMaps[index].shape[0] - 1)) {
                    inputTensor25.push(0)


                }
                else {
                    if (this.maps[dataActueleIndex + j] === undefined) inputTensor25.push(0)
                    else inputTensor25.push(this.maps[dataActueleIndex + j])
                }
                indexFive++
            }
        }
        return { inputTensor9, inputTensor25 }
    }

    getInputs(activMaps, index, position) {
        let inputTensorFull3 = []
        let inputTensorFull5 = []
        inputTensorFull3.push(this.getOneMap(activMaps, index, position).inputTensor9)
        inputTensorFull5.push(this.getOneMap(activMaps, index, position).inputTensor25)
        activMaps[index].dispose()

        return {
            inputs3: inputTensorFull3,
            inputs5: inputTensorFull5,

        }
    }

    gridThreeGenerate(filters, keyBoard) {

        Array.from(keyBoard).forEach((el, index) => {
            const cage = document.createElement('table')
            const lut = new LutHtml();
            const dataPixel = [...filters]
            const newDataPixel = []
            for (let i = 0; i < dataPixel.length; i++) {
                if (dataPixel[i] == undefined) newDataPixel.push(0)
                else newDataPixel.push(dataPixel[i])
            }

            const colors = lut.createColor(newDataPixel, "grayscale")
            let id = 0
            for (let i = 1; i <= 3; i++) {
                let tr = cage.insertRow(0)
                for (let j = 1; j <= 3; j++) {
                    let td = tr.insertCell()
                    const valueDisplay = filters[id].toFixed(2)
                    td.innerHTML = valueDisplay
                    td.style.color = 'white'
                    const intensity = Math.floor(50 + 50 * valueDisplay)
                    if (intensity > 70) {
                        td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
                    }
                    td.style.backgroundColor = `rgb(${colors[id].r * 255}, ${colors[id].g * 255}, ${colors[id].b * 255})`
                    td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
                    id++
                }
            }
            el.appendChild(cage)
        })


    }
    checkmaps(maps) {

        const mapsClear = []
        maps.forEach(el => {
            if (el == undefined) {
                mapsClear.push(0)
            }
            else mapsClear.push(el)
        })
        return mapsClear
    }

    getResultAll(maps, filter) {
        let map
        if (maps[0].includes(undefined)) map = this.checkmaps(maps[0])
        else map = this.checkmaps(maps[0])
        const result = this.sumAndMulArray(map, filter)
        let tensor = result.sum
        let sum = result.result
        document.getElementById('single-value-sum-1-dw').innerText = sum.toFixed(2)
        return tensor

    }

}

export default DeptWiseConvExplained