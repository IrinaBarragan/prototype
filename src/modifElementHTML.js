export function switchButton3D2D(three, two) {
    if (three) {
        document.querySelector('#click2D').style.backgroundColor = "#22b573"
        document.querySelector('#click2D').style.color = "white"
        document.querySelector('#click3D').style.backgroundColor = "white"
        document.querySelector('#click3D').style.color = "black"
    }
    if (two) {
        document.querySelector('#click3D').style.backgroundColor = "#22b573"
        document.querySelector('#click3D').style.color = "white"
        document.querySelector('#click2D').style.backgroundColor = 'white'
        document.querySelector('#click2D').style.color = "black"
    }
}

export async function getDWCnnModal(weights, activCards, activPreviousLayer, modal, intersectIndex, position, modelLayersName) {
    const inputsPreviousLayerOne = modal.getInputs(activPreviousLayer, 0, position)
    const inputsPreviousLayerTwo = modal.getInputs(activPreviousLayer, activPreviousLayer.length - 1, position)
    const inputs = modal.getInputs(activCards, intersectIndex, position)
    const filter = await modal.getFiltersForDeptWise(weights, intersectIndex)
    const multInputFilter = modal.getResultAll(inputs.inputs3, filter)
    modal.displayModal(inputsPreviousLayerOne, inputsPreviousLayerTwo, inputs, filter, multInputFilter, modelLayersName)
    document.getElementById('layerPrevioslyLight').innerHTML = activPreviousLayer.length + ' images ' + activPreviousLayer[0].shape[0] + ' x ' + activPreviousLayer[0].shape[0]
    activCards.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })
    activPreviousLayer.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })


}


export async function getCnnModal(weights, activCards, modal, intersectIndex, position) {
    const inputs = modal.getInputs(activCards, position)
    const filters = await modal.getFilters(position, weights, intersectIndex)
    const result = modal.getResult(inputs.inputs3, filters)
    modal.displayModal(inputs, filters, result)
    activCards.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })

}

export async function getPointCnnModal(weights, activCards, activPreviousLayer, modal, intersectIndex, position, activCardsForBN, activCardsForRelu, modelLayersName_one, modelLayersName_two) {

    document.getElementById('layerPrevioslyLight_pc').innerHTML = activPreviousLayer.length + ' images ' + activPreviousLayer[0].shape[0] + ' x ' + activPreviousLayer[0].shape[0]
    document.getElementById('layerPrevioslyLight_pc2').innerHTML = activCards.length + ' images ' + activCards[0].shape[0] + ' x ' + activCards[0].shape[0]

    const inputsPreviousLayerOne = tf.tidy(() => {
        return modal.getInputs(activPreviousLayer, 0, position)
    })
    const inputsPreviousLayerTwo = modal.getInputs(activPreviousLayer, activPreviousLayer.length - 1, position)
    const inputsFirst = modal.getInputs(activCards, 0, position)
    const inputsLast = modal.getInputs(activCards, activCards.length - 1, position)
    const filters = await modal.getFilters(position, weights, intersectIndex)
    const result = modal.getResult(activCards, filters, position)
    const valueBn = modal.getValueBN(activCardsForBN, intersectIndex, position)
    const valueRelu = modal.getValueBN(activCardsForRelu, intersectIndex, position)
    modal.displayModal(inputsPreviousLayerOne, inputsPreviousLayerTwo, inputsFirst, inputsLast, filters, result, valueBn, valueRelu, modelLayersName_one, modelLayersName_two)
    activCards.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })
    activPreviousLayer.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })
    activCardsForBN.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })
    activCardsForRelu.forEach(el => {
        try {
            el.dispose()
        }
        catch (error) {
            console.log(error)
        }
    })


}

