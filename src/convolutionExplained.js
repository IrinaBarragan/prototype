import LutHtml from './lutFotHtml'

class ConvolutionExplained {

    constructor() {
        this.modalIsDisplay = false
        window.addEventListener('resize', () => {
            this.drawLines()
        })
        this.min = 100
        this.max = -100
    }

    drawLineForSum(name, options = { width: 40, drawPlus: false }) {
        const width = options.width
        const lineName = `${name}-line`
        const line = document.getElementById(lineName)
        if (line === null) {
            throw Error(`can't find line element for "${name}", expected an element with id "${lineName}"`)
        }
        const boxName = `single-box-sum-3`
        const box = document.getElementById(boxName);
        if (box === null) {
            throw Error(`can't find box element for "${name}", expected an element with id "${boxName}"`)
        }
        const rect = box.getBoundingClientRect()
        var x1 = rect.right
        var y1 = rect.top + (rect.bottom - rect.top) / 2
        var x2 = x1 + width
        var y2 = y1

        line.setAttribute('x1', x1)
        line.setAttribute('y1', y1)
        line.setAttribute('x2', x2)
        line.setAttribute('y2', y2)

        if (options.drawPlus === true) {
            const plusName = `${name}-plus`
            const plus = document.getElementById(plusName)
            if (plus === null) {
                throw Error(`can't find plus element for "${name}", expected an element with id "${plusName}"`)
            }
            plus.setAttribute('x', x1 - plus.getBoundingClientRect().width / 2 + width / 2)
            plus.setAttribute('y', y1 - plus.getBoundingClientRect().height / 2)
        }
    }

    drawVerticalLine(name1, name2, options = { width: 40 }) {
        const width = options.width
        const lineName = `vertical-line`
        const line = document.getElementById(lineName)
        if (line === null) {
            throw Error(`can't find element for vertical line, expected an element with id "${lineName}"`)
        }
        const box1Name = `single-box-${name1}`
        const box1 = document.getElementById(box1Name);
        if (box1 === null) {
            throw Error(`can't find box element for "${name1}", expected an element with id "${box1Name}"`)
        }
        const box2Name = `single-box-${name2}`
        const box2 = document.getElementById(box2Name);
        if (box2 === null) {
            throw Error(`can't find box element for "${name2}", expected an element with id "${box2Name}"`)
        }
        const rect1 = box1.getBoundingClientRect()
        const rect2 = box2.getBoundingClientRect()
        var x1 = rect1.right + width
        var y1 = rect1.top + (rect1.bottom - rect1.top) / 2
        var x2 = rect2.right + width
        var y2 = rect2.top + (rect2.bottom - rect2.top) / 2

        line.setAttribute('x1', x1)
        line.setAttribute('y1', y1)
        line.setAttribute('x2', x2)
        line.setAttribute('y2', y2)

        const plus1Name = `${name1}-plus`
        const plus1 = document.getElementById(plus1Name)
        if (plus1 === null) {
            throw Error(`can't find plus element for "${name1}", expected an element with id "${plus1Name}"`)
        }
        plus1.setAttribute('x', x1 - plus1.getBoundingClientRect().width / 2)
        plus1.setAttribute('y', y1 + (y2 - y1) / 4 - plus1.getBoundingClientRect().height / 2)

        const plus2Name = `${name2}-plus`
        const plus2 = document.getElementById(plus2Name)
        if (plus2 === null) {
            throw Error(`can't find plus element for "${name2}", expected an element with id "${plus2Name}"`)
        }
        plus2.setAttribute('x', x1 - plus2.getBoundingClientRect().width / 2)
        plus2.setAttribute('y', y1 + (y2 - y1) * 3 / 4 - plus2.getBoundingClientRect().height / 2)
    }

    drawLines_old() {
        this.drawLineForSum('sum-1', { width: 45, drawPlus: false })
        this.drawLineForSum('sum-2', { width: 45, drawPlus: true })
        this.drawLineForSum('sum-3', { width: 45, drawPlus: false })
        this.drawVerticalLine('sum-1', 'sum-3', { width: 45 })
    }

    drawLines() {
        const linesMain = document.getElementById('conv-final-lines-main')

        const parent = document.getElementById('modalConvolution')
        const parentRect = parent.getBoundingClientRect()

        const box1 = document.getElementById('single-box-sum-1')
        const rect1 = box1.getBoundingClientRect()

        const box3 = document.getElementById('single-box-sum-3')
        const rect3 = box3.getBoundingClientRect()

        linesMain.style.top = `${rect1.top - parentRect.top + rect1.height * 1.5}px`
        linesMain.style.left = `${rect1.right - parentRect.left}px`
        linesMain.style.height = `${rect3.top - rect1.top}px`
    }

    displayModal(inputs, filters) {
        document.querySelector('#convolution').style.display = 'block'
        // document.querySelector('#conv-final-sum-lines').style.display = 'block'
        this.modalIsDisplay = true
        const keyBoardF = document.getElementsByClassName('gridFive')
        const keyBoardT = document.getElementsByClassName('gridThree')
        const keyBoardM = document.getElementsByClassName('gridMultiplication')

        this.gridFiveGenerate(inputs.inputs5, keyBoardF)
        this.gridThreeGenerate(filters, keyBoardT)
        const results = this.getResult(inputs.inputs3, filters)
        const tensorsMult = results.tensorsMult
        this.gridMultGenerate(tensorsMult, keyBoardM)
        this.drawLines()
        document.querySelector("#hideConvModal").addEventListener("click", () => this.hideModal())

    }

    getModalState() {
        return this.modalIsDisplay
    }

    hideModal() {
        document.querySelector('#convolution').style.display = 'none'
        // document.querySelector('#conv-final-sum-lines').style.display = 'none'
        let els = document.getElementsByClassName('gridThree')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridFive')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridMultiplication')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }

    async getFilters(position, weights, index) {
        document.querySelector('#convolution').style.display = 'block'
        // document.querySelector('#conv-final-sum-lines').style.display = 'block'
        const shapeWeights = { x: weights.shape[0], y: weights.shape[1], f: weights.shape[2], g: weights.shape[3] }

        const layerWeightsList = await tf.tidy(() => {
            return tf.unstack(weights.reshape([shapeWeights.x, shapeWeights.y, shapeWeights.f, shapeWeights.g]), 2)
        })

        const layerListR = await tf.tidy(() => {
            return tf.unstack(layerWeightsList[0].reshape([shapeWeights.x, shapeWeights.y, shapeWeights.g]), 2)
        });
        const layerListG = await tf.tidy(() => {
            return tf.unstack(layerWeightsList[1].reshape([shapeWeights.x, shapeWeights.y, shapeWeights.g]),
                2)
        });
        const layerListB = await tf.tidy(() => {
            return tf.unstack(layerWeightsList[2].reshape([shapeWeights.x, shapeWeights.y, shapeWeights.g]),
                2)
        });
        const filterR = tf.tidy(() => { return tf.reverse2d(layerListR[index]).dataSync() })
        const filterG = tf.tidy(() => { return tf.reverse2d(layerListG[index]).dataSync() })
        const filterB = tf.tidy(() => { return tf.reverse2d(layerListB[index]).dataSync() })
        layerListR.forEach(el=>el.dispose())
        layerListG.forEach(el=>el.dispose())
        layerListB.forEach(el=>el.dispose())
        layerWeightsList.forEach(el=>el.dispose())

        return { filterR, filterG, filterB }

    }

    getInputs(activMaps, position) {
        activMaps.forEach(el => {
            const max = Math.max(...el.dataSync())
            const min = Math.min(...el.dataSync())
            if (this.max < max) this.max = max
            if (this.min > min) this.min = min

        })

        let yIndex = position.row * 2
        let xIndex = position.col * 2
        let inputTensorFull3 = []
        let inputTensorFull5 = []
        activMaps.forEach((el, index) => {
            const l = tf.tidy(() => { return tf.reverse2d(activMaps[index]).dataSync() })
            let dataActueleIndex = 0;
            let inputTensor9 = [];

            for (let i = 0; i < 3; i++) {
                dataActueleIndex = (yIndex * 225) + (xIndex) + (i * 225)

                for (let j = 0; j < 3; j++) {
                    inputTensor9.push(l[dataActueleIndex + j])
                }
            }
            inputTensorFull3.push(inputTensor9)

            let inputTensor25 = [];
            let indexFive = 0
            for (let i = -1; i < 4; i++) {
                dataActueleIndex = (yIndex * 225) + (xIndex) + (i * 225)
                for (let j = -1; j < 4; j++) {
                    if ((indexFive % 5 < 1 && position.col === 0) ||(indexFive % 5 ==4 && position.col === 111)) {
                        inputTensor25.push(-100)

                    }
                    else {
                        if(l[dataActueleIndex + j] ===undefined) inputTensor25.push(-100)
                        else inputTensor25.push(l[dataActueleIndex + j])

                    }
                    indexFive++
                }
            }
            inputTensorFull5.push(inputTensor25)
    
        })
        activMaps.forEach(el => { 
            el.dispose() })
      
        return {
            inputs3: inputTensorFull3,
            inputs5: inputTensorFull5
        }
    }


    sumAndMulArray(a, b) {
        const aT = tf.tensor(a)
        const bT = tf.tensor(b)
        const sumTensor = aT.mul(bT)
        const sum = sumTensor.dataSync()
        aT.dispose()
        bT.dispose()
        sumTensor.dispose()
        const result = sum.reduce((previousValue, currentValue) => previousValue + currentValue)
        return { result, sum }
    }

    getResult(inputs, filters) {
        const tensorsMult = []
        let tensorR = this.sumAndMulArray(inputs[0], filters.filterR).sum
        let tensorG = this.sumAndMulArray(inputs[1], filters.filterG).sum
        let tensorB = this.sumAndMulArray(inputs[2], filters.filterB).sum
        tensorsMult.push(tensorR, tensorG, tensorB)
        let sum1 = this.sumAndMulArray(inputs[0], filters.filterR).result
        document.getElementById('single-value-sum-1').innerText = sum1.toFixed(2)
        let sum2 = this.sumAndMulArray(inputs[1], filters.filterG).result
        document.getElementById('single-value-sum-2').innerText = sum2.toFixed(2)
        let sum3 = this.sumAndMulArray(inputs[2], filters.filterB).result
        document.getElementById('single-value-sum-3').innerText = sum3.toFixed(2)
        let result = sum1 + sum2 + sum3
        document.getElementById('single-value-output').innerText = result.toFixed(2)

        return { result, tensorsMult }
    }

    gridFiveGenerate(inputs, keyBoard, type, centerCell="false") {
        Array.from(keyBoard).forEach((el, index) => {
            const lut = new LutHtml();
            let colors 
            if(type=='pointWise' || type=='depthWise' ) colors= lut.createColor(inputs[index])
            else colors= lut.createColorRGB(inputs[index], this.max, this.min)
            const cage = document.createElement('table')
            cage.style.width = '150px'
            let id = 0
            let k = 0;
            const array = [1, 2, 3, 4, 5]
            let inputIndex = 0
            for (let i = 1; i <= 5; i++) {
                let tr = cage.insertRow(0)
                for (let j = 1; j <= 5; j++) {
                    let td = tr.insertCell()
                    if ((array.includes(i)) && (array.includes(j))) {
                        let valueDisplay
                        if (inputs[index][inputIndex] === undefined) valueDisplay = 'N'
                        else valueDisplay = inputs[index][inputIndex].toFixed(2)
                        if ([2, 3, 4].includes(i) && [2, 3, 4].includes(j)) {
                            td.innerHTML = valueDisplay
                            if(type=='pointWise'&& i===3 && j===3 && centerCell===true) {
                                td.style.border = "solid 5px green"
                            }
                            // if (valueDisplay > .85) td.style.color = "#3d3d3d"
                        }
                        if(type=='pointWise' || type=='depthWise') this.fillCells(index, colors, td, k)
                        else this.fillCellsRGB(index, colors, td, k)
                 
                        inputIndex++
                        k++
                    }

                }
            }
            el.appendChild(cage)
        })


    }
    fillCellsRGB(index, colors, td, k){
        if (index == 0) td.style.backgroundColor = `rgb(${colors[k] * 255}, 0, 0)`
        if (index == 1) td.style.backgroundColor = `rgb(0, ${colors[k] * 255}, 0)`
        if (index == 2) td.style.backgroundColor = `rgb(0, 0, ${colors[k] * 255})`
    }

    fillCells(index, colors, td, k){
        td.style.backgroundColor = `rgb(${colors[k].r * 255}, ${colors[k].g * 255}, ${colors[k].b * 255})`
        td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
    }

    gridThreeGenerate(filters, keyBoard) {
        const arrayFilters = []
        arrayFilters.push(filters.filterR)
        arrayFilters.push(filters.filterG)
        arrayFilters.push(filters.filterB)

        Array.from(keyBoard).forEach((el, index) => {
            const filter = arrayFilters[index]
            const cage = document.createElement('table')
            let id = 0
            const lut = new LutHtml();
            const colors = lut.createColor(filter, "grayscale")
            for (let i = 1; i <= 3; i++) {
                let tr = cage.insertRow(0)
                for (let j = 1; j <= 3; j++) {
                    let td = tr.insertCell()
                    const valueDisplay = filter[id].toFixed(2)
                    td.innerHTML = valueDisplay
                    td.style.color = 'white'

                    const intensity = Math.floor(50 + 50 * valueDisplay)
                    if (intensity > 70) {
                        td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
                    }
                    td.style.backgroundColor = `rgb(${colors[id].r * 255}, ${colors[id].g * 255}, ${colors[id].b * 255})`
                    id++
                }
            }
            el.appendChild(cage)
        })


    }

    gridMultGenerate(tensor, keyBoard) {
        Array.from(keyBoard).forEach((el, index) => {
            const array = tensor[index]
            const cage = document.createElement('table')
            let id = 0
            for (let i = 1; i <= 3; i++) {
                let tr = cage.insertRow(0)
                for (let j = 1; j <= 3; j++) {
                    let td = tr.insertCell()
                    const valueDisplay = array[id].toFixed(2)
                    td.innerHTML = valueDisplay
                    const intensity = Math.floor(50 + 100 * valueDisplay)
                    if (intensity > 70) {
                        td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
                    }
                    td.style.backgroundColor = `hsl(0 0% ${intensity}%)`
                    id++
                }
            }
            el.appendChild(cage)
        })

    }
    logoNameLayerManager(el) {
        let src = false
        if (el.includes('_conv') || el.includes('_expand') || el.includes('_project')) src = 'icon_conv.png'
        if (el.includes('_depthwise')) src = 'icon_convdw.png'
        if (el.includes('_relu') || el.includes('relu_')) src = 'icon_relu.png'
        if (el.includes('_BN') || el.includes('bn_') || el.includes('_bn')) src = 'icon_bn.png'
        if (el.includes('_add')) src = 'icon_add.png'
        if (el.includes('_pad')) src = 'icon_zero_pad.png'

        return src
    }
blockLayersName(modelLayersName, list){
    let src
    modelLayersName.forEach(el => {
        this.logoNameLayerManager(el)
        let newEl
        if (this.logoNameLayerManager(el) == false) {
            newEl = document.createElement('p')
            newEl.innerHTML = el
        }
        else {
            newEl = document.createElement('img')
            src = this.logoNameLayerManager(el)
            newEl.src = src
            newEl.style.height = '40px'
            const elClass = src.split('.')[0]
            newEl.setAttribute('class', elClass)
        }


        list.appendChild(newEl)
 
    })
}

}

export default ConvolutionExplained