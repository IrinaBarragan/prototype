import { DataTexture, RGBAFormat, UnsignedByteType } from 'three'
import { Lut } from 'three/examples/jsm/math/Lut.js'

function getMaxMin(data) {
  let max = -1000
  let min = 1000
  for (let i = 0; i < data.length; i++) {
    const dataPixel = data[i]
    if (dataPixel > max) {
      max = dataPixel
    }
    if (dataPixel < min) {
      min = dataPixel
    }
  }
  return { max, min }
}

function getDataTexture(shape, maps) {
  const maxGlobal = getMaxMin(maps).max
  const minGlobal = getMaxMin(maps).min
  const max = getMaxMinAroundZerro(maxGlobal, minGlobal).max
  const min = getMaxMinAroundZerro(maxGlobal, minGlobal).min

  const lut = new Lut('grayscale', 512);
  let size = shape * shape


  //   dataImage
  const data = new Uint8Array(4 * size);
  for (let i = 0; i < size; i++) {

    let imgNormalize = (maps[i] - min) / (max - min)
    const color = lut.getColor(imgNormalize)
    let stride = i * 4
    data[stride] = color.r * 255// red, and also X
    data[stride + 1] = color.g * 255// gree, and also Y
    data[stride + 2] = color.b * 255// blue
    data[stride + 3] = 255; // opacité
  }
  const dataTexture = new DataTexture(data, shape, shape, RGBAFormat, UnsignedByteType);
  dataTexture.needsUpdate = true;
  return dataTexture
}

function getMaxMinAroundZerro(max, min) {
  const absMin = Math.abs(min)
  const absMax = Math.abs(max)
  max = absMax > absMin ? absMax : absMin
  min = -max
  return { max, min }
}

export default getDataTexture