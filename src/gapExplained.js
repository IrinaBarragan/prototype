import LutHtml from './lutFotHtml'

class GapExplained {

    constructor() {
        this.modalIsDisplay = false


    }


    displayModal(inputs) {
        document.querySelector('#average').style.display = 'block'
        // document.querySelector('#conv-final-sum-lines').style.display = 'block'
        this.modalIsDisplay = true
        this.gridSevenGenerate(inputs)
        const sommResult = inputs.reduce((a, b) => a + b)
        document.querySelector('#gap-value-sum-49').innerHTML = sommResult.toFixed(2)
        document.querySelector('#moyenne').innerHTML = (sommResult / 49).toFixed(2)
        document.querySelector("#hideGapModal").addEventListener("click", () => this.hideModal())

    }

    getModalState() {
        return this.modalIsDisplay
    }

    hideModal() {
        document.querySelector('#average').style.display = 'none'
        let els = document.getElementsByClassName('gridSeven')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }

    getInputs(activMaps, position) {
        const index = 1280 - (position.row + 1280 / 2) - 1
        const input = activMaps[index].dataSync()
        let inputTensor49 = []
        input.forEach((el) => {
            inputTensor49.push(el)
        })
        activMaps.forEach(el => {
            el.dispose()
        })
        return inputTensor49.reverse()
    }
    gridSevenGenerate(inputs) {
        const lut = new LutHtml();
        const colors = lut.createColor(inputs)
        const keyBoard = document.getElementsByClassName('gridSeven')
        Array.from(keyBoard).forEach((el, index) => {
            const cage = document.createElement('table')
            const array = [1, 2, 3, 4, 5, 6, 7]
            let inputIndex = 0
            let k = 0
            for (let i = 1; i <= 7; i++) {
                let tr = cage.insertRow(0)
                for (let j = 1; j <= 7; j++) {
                    let td = tr.insertCell()
                    if ((array.includes(i)) && (array.includes(j))) {
                        let valueDisplay = inputs[inputIndex].toFixed(1)
                        valueDisplay == 0 ? valueDisplay = 0 : valueDisplay
                        if (array.includes(i) && array.includes(j)) {
                            td.innerHTML = valueDisplay


                            td.style.backgroundColor = `rgb(${colors[k].r*255}, ${colors[k].g*255}, ${colors[k].b*255})`
                            td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'
                            k++
                            inputIndex++

                        }

                    }
                }
            }
            el.appendChild(cage)
        })


    }




}

export default GapExplained