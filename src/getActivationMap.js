
// capture, cropTp and cropTensor are the function from customMobilNet use for prediction of one image
// have transformed from ts
function capture(image) {
    return tf.tidy(() => {
        const pixels = tf.browser.fromPixels(image);
        // crop the image so we're using the center square
        const cropped = cropTensor(pixels, false, false);

        // Expand the outer most dimension so we have a batch size of 1
        const batchedImage = cropped.expandDims(0);
        pixels.dispose()
        cropped.dispose()

        // Normalize the image between -1 and a1. The image comes in between 0-255
        // so we divide by 127 and subtract 1.
        return batchedImage.toFloat().div(tf.scalar(127)).sub(tf.scalar(1));
        // return batchedImage.toFloat().div(tf.scalar(127)).sub(tf.scalar(1));

        batchedImage.dispose()

    });
}

function cropTo(image, size, flipped) {
    let canvas = document.createElement("canvas")
    // image image, bitmap, or canvas
    let width = image.width;
    let height = image.height;


    const min = Math.min(width, height);
    const scale = size / min;
    const scaledW = Math.ceil(width * scale);
    const scaledH = Math.ceil(height * scale);
    const dx = scaledW - size;
    const dy = scaledH - size;
    canvas.width = canvas.height = size;
    let ctx = canvas.getContext('2d');
    ctx.drawImage(image, ~~(dx / 2) * -1, ~~(dy / 2) * -1, scaledW, scaledH);

    // canvas is already sized and cropped to center correctly
    if (flipped) {
        ctx.scale(-1, 1);
        ctx.drawImage(canvas, size * -1, 0);
    }
    return canvas;
}

function cropTensor(img, grayscaleModel, grayscaleInput) {
    const size = Math.min(img.shape[0], img.shape[1]);
    const centerHeight = img.shape[0] / 2;
    const beginHeight = centerHeight - (size / 2);
    const centerWidth = img.shape[1] / 2;
    const beginWidth = centerWidth - (size / 2);

    if (grayscaleModel && !grayscaleInput) {
        //cropped rgb data
        let grayscale_cropped = img.slice([beginHeight, beginWidth, 0], [size, size, 3]);
        grayscale_cropped = grayscale_cropped.reshape([size * size, 1, 3])
        const rgb_weights = [0.2989, 0.5870, 0.1140]
        grayscale_cropped = tf.mul(grayscale_cropped, rgb_weights)
        grayscale_cropped = grayscale_cropped.reshape([size, size, 3]);
        grayscale_cropped = tf.sum(grayscale_cropped, -1)
        grayscale_cropped = tf.expandDims(grayscale_cropped, -1)
        return grayscale_cropped;
        grayscale_cropped.dispose()
    }
    return img.slice([beginHeight, beginWidth, 0], [size, size, 3]);
}



export async function getActivCardsAndWeights(image, model, flip=false) {


    //  cropTo cree un canvas virtuel ou draw l'image
    const imageTensor = tf.tidy(() => {
        const croppedImage = cropTo(image, 224, flip);
        const captured = capture(croppedImage, flip);
        return captured
    })


    const prediction = await model.predict(image);
    // array for activation cards
    const activationCards = [];
    const layersWeights = [];
    for (let n = 0; n < activationCards.length; n++) {
        activationCards[n].dispose()
    }
    for (let n = 0; n < layersWeights.length; n++) {
        layersWeights[n].dispose()
    }

    // let imageFromPixel = await tf.browser.fromPixels(image)
    // const feedImage = tf.image.resizeBilinear(imageFromPixel, [200, 200]);
    // const imageTensor1 = tf.expandDims(feedImage, 0).asType('float32').div(255.0)

    //  push first layer - input
    activationCards.push(imageTensor)

    return tf.tidy(() => {
        // let layer
        layersWeights.push(model.model.layers[0].layers[0].layers[0].getWeights())
        let layer;

        for (let i = 1; i <= 27; i++) {

            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 27) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
               
            
            } else {
                let activationCard27 = model.model.layers[0].layers[0].layers[27].apply([activationCards[26], activationCards[18]])
                // let activationCard27 = activationCards[26].add(activationCards[18])
                activationCards.push(activationCard27)
               
                
            }
        }


        for (let i = 28; i <= 45; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 45) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard45 = model.model.layers[0].layers[0].layers[45].apply([activationCards[44], activationCards[36]])
                // let activationCard45 = activationCards[44].add(activationCards[36])
                activationCards.push(activationCard45)
            }
        }


        for (let i = 46; i <= 54; i++) {
            layersWeights.push(model.model.model.layers[i].getWeights()[0])
            if (i < 54) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard54 = model.model.layers[0].layers[0].layers[54].apply([activationCards[53], activationCards[45]])
                // let activationCard54 = activationCards[53].add(activationCards[45])
                activationCards.push(activationCard54)
            }
        }


        for (let i = 55; i <= 72; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 72) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard72 = model.model.layers[0].layers[0].layers[72].apply([activationCards[71], activationCards[63]])
                // let activationCard72 = activationCards[71].add(activationCards[63])
                activationCards.push(activationCard72)
            }
        }


        for (let i = 73; i <= 81; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 81) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard81 = model.model.layers[0].layers[0].layers[81].apply([activationCards[80], activationCards[72]])
                // let activationCard81 = activationCards[80].add(activationCards[72])
                activationCards.push(activationCard81)
            }
        }

        for (let i = 82; i <= 90; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 90) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard90 = model.model.layers[0].layers[0].layers[90].apply([activationCards[89], activationCards[81]])
                // let activationCard90 = activationCards[89].add(activationCards[81])
                activationCards.push(activationCard90)
            }
        }

        for (let i = 91; i <= 107; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 107) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard107 = model.model.layers[0].layers[0].layers[107].apply([activationCards[106], activationCards[98]])
                // let activationCard107 = activationCards[106].add(activationCards[97])
                activationCards.push(activationCard107)
            }
        }

        for (let i = 108; i <= 116; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 116) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard116 = model.model.layers[0].layers[0].layers[116].apply([activationCards[115], activationCards[107]])
                // let activationCard116 = activationCards[115].add(activationCards[107])
                activationCards.push(activationCard116)
            }
        }

        for (let i = 117; i <= 134; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 134) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard134 = model.model.layers[0].layers[0].layers[134].apply([activationCards[133], activationCards[125]])
                // let activationCard134 = activationCards[133].add(activationCards[125])
                activationCards.push(activationCard134)
            }
        }

        for (let i = 135; i <= 143; i++) {
            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            if (i < 143) {
                layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
                activationCards.push(layer);
            } else {
                let activationCard143 = model.model.layers[0].layers[0].layers[143].apply([activationCards[134], activationCards[142]])
                // let activationCard143 = activationCards[134].add(activationCards[142])
                activationCards.push(activationCard143)
            }
        }

        for (let i = 144; i <= 154; i++) {

            layersWeights.push(model.model.layers[0].layers[0].layers[i].getWeights()[0])
            layer = model.model.layers[0].layers[0].layers[i].apply(activationCards[i - 1])
            activationCards.push(layer);
        }


        let layer155 = model.model.layers[0].layers[1]
        layersWeights.push(layer155.getWeights())
        let activationCard155 = layer155.apply(activationCards[154])
        activationCards.push(activationCard155)
     
        let layer156 = model.model.layers[1].layers[0]
        layersWeights.push(layer156.getWeights())
        let activationCard156 = layer156.apply(activationCards[155])
        activationCards.push(activationCard156)


        let layer157 = model.model.layers[1].layers[1]

        layersWeights.push(layer157.getWeights())
        let activationCard157 = layer157.apply(activationCards[156])
        activationCards.push(activationCard157)
   

        const weights157 = layersWeights[157]
        // weights of layer caterories size 100 * number of categories
        const weightslayer157 = tf.tidy(() => {
          return tf.unstack(weights157[0].reshape([weights157[0].shape[0], weights157[0].shape[1]]), 1)
        })

        return {
            activationCards,
            layersWeights
        }
      
    })
}
