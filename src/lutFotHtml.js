import { Lut } from 'three/examples/jsm/math/Lut.js'


class LutHtml {
    constructor() {
        const lut = new Lut(this.palette, 512);
    }

    createColor(dataPixel, palette = "cooltowarm", min, max) {
        const lut = new Lut(palette, 512);
        const colors = []
        if(!max) max = Math.max(...dataPixel)+0.1
        if(!min) min = Math.min(...dataPixel)-0.1
        if (palette === "cooltowarm") min = -max
        //   dataImage
        const data = new Uint8Array(4 * dataPixel.length);
        for (let i = 0; i < dataPixel.length; i++) {
            let imgNormalize = (dataPixel[i] - min) / (max - min)
            const color = lut.getColor(imgNormalize)
            colors.push(color)
        }
        return colors
    }

    createColorForOne(dataPixel, palette) {
        const lut = new Lut(palette, 512);
        const colors = []
        //   dataImage
        const data = new Uint8Array(4 * dataPixel.length);
   
            const color = lut.getColor(dataPixel)
            colors.push(color)
 
        return colors
    }

    createColorRGB(dataPixel, max, min) {
        const colors = []
        max ? max : Math.max(...dataPixel) + 0.1
        min ? min : Math.min(...dataPixel) - 0.1
        //   dataImage
        const data = new Uint8Array(4 * dataPixel.length);
        for (let i = 0; i < dataPixel.length; i++) {

            let imgNormalize = (dataPixel[i] - min) / (max - min)
            const color = imgNormalize
            colors.push(color)
        }
        return colors
    }

}

export default LutHtml