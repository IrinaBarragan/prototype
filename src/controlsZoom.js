import * as THREE from "three";


class ZoomMouseControls {
    constructor(camera, container) {
        this.enabled = true;
        this.camera = camera
        this.container = container
    }

    update() {
        this.camera.updateProjectionMatrix();
    }

    zoomToPosition(factor, position, camera, container) {
        if (this.enabled === false) return
        this.container = container
        this.camera = camera

        let worldDirection = new THREE.Vector3();
        this.camera.getWorldDirection(worldDirection);


        let zoomDirection = new THREE.Vector3(position.x, position.y, (this.camera.near + this.camera.far) / (this.camera.near - this.camera.far))
            .unproject(this.camera);
        zoomDirection.sub(this.camera.position)
        const aspectRatio = this.container.clientHeight / this.container.clientWidth

        // const aspectRatio = totalY / totalX;
        let from = this.camera.left - zoomDirection.x;
        let ratio = from / this.container.clientWidth
        this.camera.left -= ratio * factor;

        from = this.camera.right - zoomDirection.x;
        ratio = from / this.container.clientWidth
        this.camera.right -= ratio * factor;

        from = this.camera.top - zoomDirection.y;
        ratio = from / this.container.clientHeight;
        this.camera.top -= ratio * factor * aspectRatio;

        from = this.camera.bottom - zoomDirection.y;
        ratio = from / this.container.clientHeight;
        this.camera.bottom -= ratio * factor * aspectRatio;
        this.camera.updateProjectionMatrix();
        position.set(0, 0, position.z)

        // this.camera.position.set(0, 0, position.z)
        //  this.update()

    }

    zoomToCenter(position, camera, container, width) {
        return
        let factor = this.getFactor(width)
        if (this.enabled === false) return
        this.container = container
        this.camera = camera
        let worldDirection = new THREE.Vector3();
        this.camera.getWorldDirection(worldDirection);

        let zoomDirection = new THREE.Vector3(position.x, position.y, (this.camera.near + this.camera.far) / (this.camera.near - this.camera.far))
            .unproject(this.camera);
        zoomDirection.sub(this.camera.position)
        const aspectRatio = this.container.clientHeight / this.container.clientWidth

        // const aspectRatio = totalY / totalX;
        let from = this.camera.left - zoomDirection.x;
        let ratio = from / this.container.clientWidth
        this.camera.left -= ratio * factor;

        from = this.camera.right - zoomDirection.x;
        ratio = from / this.container.clientWidth
        this.camera.right -= ratio * factor;

        from = this.camera.top - zoomDirection.y;
        ratio = from / this.container.clientHeight;
        this.camera.top -= ratio * factor * aspectRatio;

        from = this.camera.bottom - zoomDirection.y;
        ratio = from / this.container.clientHeight;
        this.camera.bottom -= ratio * factor * aspectRatio;
        if(this.camera.zoom <0) return
        this.camera.zoom = 1

        this.camera.updateProjectionMatrix();
    }

    getFactor(width) {

        let factor = 1.5;
        this.camera.zoom = 1
        switch (Math.floor(width)) {
            case 7:
                factor = 1400;

                break
            case 14:
                factor = 1350;
                break
            case 28:
                factor = 1300;
                break
            case 56:
                factor = 1250;
                break
            case 112:
                factor = 1200;
                break
        }
        return factor
    }
}
export default ZoomMouseControls
