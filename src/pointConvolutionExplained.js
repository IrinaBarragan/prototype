import ConvolutionExplained from './convolutionExplained'
import LutHtml from './lutFotHtml'

class PointConvolutionExplained extends ConvolutionExplained {

    constructor() {
        super()
        this.modalIsDisplay = false
        window.addEventListener('resize', () => {
            this.drawLines()
        })

        this.filters
        this.type='pointWise'

    }



    displayModal(inputsPreviousLayerOne, inputsPreviousLayerTwo, inputsFirst, inputsLast, filters, result, valueBn, valueRelu, modelLayersName_one, modelLayersName_two ) {
        document.querySelector('#pConvolution').style.display = 'block'
        const list_one = document.querySelector('.operationsListPc_one')
        const list_two = document.querySelector('.operationsListPc_two')
        this.blockLayersName(modelLayersName_one, list_one)
        this.blockLayersName(modelLayersName_two, list_two)

        this.filters = filters
     
        this.modalIsDisplay = true
        const l = filters.length
        const keyBoardOne = document.getElementsByClassName('one-pc')
        const keyBoardTwo = document.getElementsByClassName('two-pc')
        const keyBoardFirst = document.getElementsByClassName('gridFivePCFirst')
        const keyBoardLast = document.getElementsByClassName('gridFivePCLast')
        const keyFirst = document.getElementsByClassName('gridTourPCFirst')
        const keyLast = document.getElementsByClassName('gridTourPCLast')
        this.gridFiveGenerate(inputsPreviousLayerOne.inputs5, keyBoardOne, this.type)
        this.gridFiveGenerate(inputsPreviousLayerTwo.inputs5, keyBoardTwo, this.type)
        this.gridFiveGenerate(inputsFirst.inputs5, keyBoardFirst, this.type, true)
        this.gridFiveGenerate(inputsLast.inputs5, keyBoardLast, this.type, true)
        this.oneGenerate(filters[0], keyFirst)
        this.oneGenerate(filters[l - 1], keyLast)
        document.getElementById("single-value-pc").innerHTML = result.toFixed(2)
        // document.getElementById("single-value-bn").innerHTML = valueBn.toFixed(2)
        document.getElementById("single-value-relu").innerHTML = valueRelu.toFixed(2)

        this.drawLines()
        document.querySelector("#hidePConvModal").addEventListener("click", () => this.hideModal())

    }

    hideModal() {
        document.querySelector('#pConvolution').style.display = 'none'
        let els = document.getElementsByClassName('operationsListPc_one')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('operationsListPc_two')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('one-pc')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('two-pc')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridFivePCFirst')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridFivePCLast')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridTourPCFirst')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridTourPCLast')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }

    async getFilters(position, weights, intersectIndex) {
        const shapeWeights = { x: weights.shape[0], y: weights.shape[1], f: weights.shape[2], g: weights.shape[3] }
        const layerWeightsList = await tf.tidy(() => {
            return tf.unstack(weights.reshape([shapeWeights.x, shapeWeights.y, shapeWeights.f, shapeWeights.g]), 3)
        })
        const filter = layerWeightsList[intersectIndex].dataSync()
        layerWeightsList.forEach(el=>el.dispose())
        return filter
    }


    getOneMap(activMaps, index, position) {
        const maps = tf.tidy(() => { return tf.reverse2d(activMaps[index]).dataSync() })
        let dataActueleIndex = 0;
        let inputTensor9 = [];
        let inputTensor25 = []
        let yIndex = 0
        let xIndex = 0
        const a = position.layer

        yIndex = position.row - 1
        xIndex = position.col - 1
        let indexThree = 0
        for (let i = 0; i < 3; i++) {
            dataActueleIndex = (yIndex * activMaps[index].shape[0]) + (xIndex) + (i * activMaps[index].shape[0])

            for (let j = 0; j < 3; j++) {
                // extremes
                if ((indexThree % 3 < 1 && position.col === 0) || (indexThree % 3 === 2 && position.col === activMaps[index].shape[0] - 1)) {
                    inputTensor9.push(0)


                } else {
                    if (maps[dataActueleIndex + j] === undefined) inputTensor9.push(0)
                    else inputTensor9.push(maps[dataActueleIndex + j])
                }
                indexThree++
            }

        }

        inputTensor25 = [];
        let indexFive = 0
        for (let i = -1; i < 4; i++) {
            dataActueleIndex = (yIndex * activMaps[index].shape[0]) + (xIndex) + (i * activMaps[index].shape[0])
            for (let j = -1; j < 4; j++) {
                // extremes
                if (((indexFive % 5 < 1 || indexFive % 5 == 1) && position.col === 0) || ((indexFive % 5 == 4 || indexFive % 5 == 3) && position.col === activMaps[index].shape[0] - 1)) {
                    inputTensor25.push(0)


                }
                else {
                    if (maps[dataActueleIndex + j] === undefined) inputTensor25.push(0)
                    else inputTensor25.push(maps[dataActueleIndex + j])
                }
                indexFive++
            }
        }


        return { inputTensor9, inputTensor25 }
    }


    getInputs(activMaps, index, position) {
        let inputTensorFull3 = []
        let inputTensorFull5 = []
        inputTensorFull3.push(this.getOneMap(activMaps, index, position).inputTensor9)
        inputTensorFull5.push(this.getOneMap(activMaps, index, position).inputTensor25)
        return {
            inputs3: inputTensorFull3,
            inputs5: inputTensorFull5,

        }
    }
    getValueBN(activMaps, index, position){
       const mapBNSelect = tf.tidy(() => { return tf.reverse2d(activMaps[index]) }).dataSync()
       const clickIndex = (position.row) * 7 + position.col
       const mapSelect=mapBNSelect[clickIndex]
       return mapSelect
    }


    getResult(inputs, filters, position) {
        const res = []
        const clickIndex = (position.row) * 7 + position.col

        inputs.forEach((el, index) => {
            const l = tf.tidy(() => { return tf.reverse2d(inputs[index]).dataSync() })
            res.push(l[clickIndex] * filters[index])
        })
        const init = 0
        const sum = res.reduce((a, b) => a + b, init)
        inputs.forEach(el=>el.dispose())
        return sum

    }

    oneGenerate(inputs, keyBoard) {
        const lut = new LutHtml();
        const min = -1
        const max = 1

        const colors = lut.createColorForOne(inputs, "grayscale")
        const cage = document.createElement('table')
        const tr = cage.insertRow(0)
        let td = tr.insertCell()
        td.style.width = '30px'
        const valueDisplay = inputs.toFixed(2)
        td.innerHTML = valueDisplay
        td.style.backgroundColor = `rgb(${colors[0].r * 255}, ${colors[0].g * 255}, ${colors[0].b * 255})`
        td.style.border = 'solide 2px green'
        td.style.textShadow = '-1px -1px 0 #999, 1px -1px 0 #999, -1px 1px 0 #999, 1px 1px 0 #999'

        keyBoard[0].appendChild(cage)
    }




}

export default PointConvolutionExplained