import ReluExplained from "./reluExplained"


class SoftmaxExplained extends ReluExplained{

    constructor() {
        super()
        this.results = 0
        this.result = 0
        this.modalIsDisplay = false
        this.resFinal = 0
        this.indexWeights = 0
    }


    displayModal(inputs, weights, result, allRes, output, prediction) {
        document.querySelector('#inputSoftMax').innerHTML=''
        document.querySelector('#output').innerHTML=''
        document.querySelector('#softmaxExp').style.display = 'block'
        // document.querySelector('#conv-final-sum-lines').style.display = 'block'
        const containerActivCards = document.getElementById('gridTourSM')
        const containerWeights = document.getElementById('gridTourWeightsSM')
        const containerResults = document.getElementById('gridTourResultsSM')
        this.modalIsDisplay = true
        this.gridTourGenerate(inputs, containerActivCards, 0)
        this.gridTourGenerate(weights, containerWeights, 22)
        this.gridTourGenerate(result, containerResults, 0)
        document.querySelector('#somme').innerHTML = this.result.dataSync()[0].toFixed(2)
        document.querySelector('#softmax').innerHTML = this.resFinal.toFixed(2)
        document.querySelector('#hideSoftMaxModal').addEventListener('click', () => this.hideModal())
        allRes.forEach(el=>{
            const newP = document.createElement('p')
            if(this.result.dataSync()[0] == el.dataSync()[0]) newP.style.color='red'
            document.querySelector('#inputSoftMax').appendChild(newP)
            newP.innerHTML = `${el.dataSync()[0].toFixed(2)} <br/>`
        })
        output.dataSync().forEach(el=>{
            const newP = document.createElement('p')
            if(this.result===el) newP.style.color='red'
            document.querySelector('#output').appendChild(newP)
            newP.innerHTML = `${el.toFixed(2)} <br/>`
        })
       
            document.querySelector('#inputSoftMax').style.height=`${18*allRes.length+5}px`
            document.querySelector('#output').style.height=`${18*allRes.length+5}px`
            const carre = document.querySelectorAll('.carre')
            carre.forEach(el=>el.style.height=`${18*allRes.length}px`)
            const category = prediction.filter(el=> el.probability.toFixed(4) == this.resFinal.toFixed(4))
            const winName = category.length!=0 ? category[0].className : prediction[this.indexWeights].className
        document.querySelector('#softmaxPercent').innerHTML = `${winName} ${Math.round(this.resFinal*100)}%`
    }

    hideModal() {
        document.querySelector('#softmaxExp').style.display = 'none'
        let els = document.getElementsByClassName('single-value')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridTour')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }

    getData(activMaps, weights, position) {
        const index = 100 - (position.row + 100 / 2) - 1
        const indexWeights = weights.length - (position.row + weights.length / 2) - 1
        this.indexWeights = indexWeights

    
        const input = activMaps.dataSync()
        const cardsMulWeights = weights.map(el => ((el.mul(activMaps))))
        this.resFinal = this.softMax(cardsMulWeights, indexWeights)
        const activWeights = weights[indexWeights].dataSync()
        const allResults =[] 
        for(let i=0; i<weights.length; i++){
            const res = activMaps.mul(weights[i])
            allResults.push(res.sum())
            if (i===indexWeights) {
                this.results = res
                this.result = this.results.sum()
            }
    
            
        }
     
        let input14 = []
        let weights14 = []
        let result14 = []
        let i = index - 5
        for (let i = 0; i < 5; i++) {
            input14.push(input[i])
            weights14.push(activWeights[i])
            result14.push(this.results.dataSync()[i])
        }
        for (let a = input.length - 5; a < input.length ; a++) {
            input14.push(input[a])
            weights14.push(activWeights[a])
            result14.push(this.results.dataSync()[a])

        }
        input14.reverse()
        weights14.reverse()
        result14.reverse()
        weights.forEach(el => el.dispose())

        return { input14, weights14, result14, allResults }

    }

    softMax(dataLinks, index) {
        const results = dataLinks.map(el => el.sum().dataSync()[0])
        const tensor = tf.tensor1d(results)
        const softMaxTensor = tensor.softmax()
        return softMaxTensor.dataSync()[index]
    }

}

export default SoftmaxExplained