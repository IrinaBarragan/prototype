import Observable from "./observable.js";


class Controller extends Observable {
    constructor() {

        if (typeof Controller._instance !== 'undefined') {
            return Controller._instance;
        }
        super()
        Controller._instance = this
        
    }
    init(container) {
        this.trigger('closeModuleInput')
   
        document.getElementById("image-container").addEventListener('click', () => {
            event.preventDefault()
            this.trigger('modalDisplay')
        });
        document.getElementById("closeModuleInput").addEventListener("click", () => {
            this.trigger('closeModuleInput')
        })
        document.getElementById("videoImage").addEventListener("click", () => {
            this.trigger('openModuleInput')
        })

        //header
        document.querySelector("#retour").addEventListener("click", () =>{
            this.trigger('returnToIntrainement')
        })

        // click boutton 2D/3D
        document.querySelector("#click3D").addEventListener('click', () => {
            this.trigger('click3D')
        })
        document.querySelector("#click2D").addEventListener('click', () => {
            this.trigger('click2D')

        })
        document.querySelector("#toggleFichierCamera").addEventListener('click', () => {
            this.trigger('toggleFichierCamera')

        })

        //    modal upload image

        document.getElementById("img").addEventListener('change', () => {
            this.trigger('uploadImage')
        });
        document.getElementById("closeModal").addEventListener('click', () => {
            this.trigger('closeModal')
        })


        container.addEventListener("click", (e) => {
            this.trigger('monMouseClick')
        })

       container.addEventListener("dblclick", (event) => {
            this.trigger('zoomClick')
        })

        document.querySelector("#lessZoom").addEventListener('click', () => {
            this.trigger('lessZoom')

        })
        document.querySelector("#moreZoom").addEventListener('click', () => {
            this.trigger('moreZoom')

        })
        document.getElementById("infoBlock").addEventListener('click', () => {
            this.trigger('dispayInfoBlock')
        });

        window.addEventListener('resize', () => {
            this.trigger("resize")
        })

        container.addEventListener('wheel', () => {
            this.trigger('onMouseWheel')

        })

        container.addEventListener('mousemove', () => {
            this.trigger('onMouseOver')
        })

        document.querySelector('#screenInitial').addEventListener('click', () => {
            this.trigger('screenInitial')
        })

  
        document.querySelector('#realTemps').addEventListener('click', () => {
            this.trigger('cameraSource')
        })

  

    }



}

export default Controller