

class ReluExplained {

    constructor() {
        this.results = 0
        this.resultWithoutBias = 0
        this.biasActiv = 0
        this.result = 0
        this.modalIsDisplay = false
    }


    displayModal(inputs, weights, result) {

        document.querySelector('#reluExp').style.display = 'block'
        // document.querySelector('#conv-final-sum-lines').style.display = 'block'
        const containerActivCards = document.getElementById('gridTour')
        const containerWeights = document.getElementById('gridTourWeights')
        const containerResults = document.getElementById('gridTourResults')
        const containerBias = document.getElementById('gridTourBias')

        this.modalIsDisplay = true
        this.gridTourGenerate(inputs, containerActivCards, 0)
        this.gridTourGenerate(weights, containerWeights, 22)
        this.gridTourGenerate(result, containerResults, 0)

        document.querySelector('#sum').innerHTML = this.resultWithoutBias.dataSync()[0].toFixed(2)
        document.querySelector('#andBias').innerHTML = this.biasActiv.toFixed(2)
        document.querySelector('#result').innerHTML = this.result.dataSync()[0].toFixed(2)
        this.result = tf.tidy(() => {
            return this.result.dataSync()[0] <= 0 ? 0 : this.result.dataSync()[0]})
        document.querySelector('#relu').innerHTML = this.result.toFixed(2)
        document.querySelector("#hideReluModal").addEventListener("click", () => this.hideModal())


    }

    getModalState() {
        return this.modalIsDisplay
    }

    hideModal() {

        document.querySelector('#reluExp').style.display = 'none'
        let els = document.getElementsByClassName('single-value')
        Array.from(els).forEach(el => el.innerHTML = "")
        els = document.getElementsByClassName('gridTour')
        Array.from(els).forEach(el => el.innerHTML = "")
        this.modalIsDisplay = false
    }

    getInputs(activMaps, weights, position, bias) {
     
        const indexWeights = 100 - (position.row + 100 / 2) - 1
        const input = tf.tidy(() => activMaps.dataSync())
        const activWeights = weights[indexWeights].dataSync()
        this.biasActiv = bias[indexWeights]
        this.results = activMaps.mul(weights[indexWeights])
        this.resultWithoutBias = this.results.sum()
        this.result = this.resultWithoutBias.add(this.biasActiv)

        let input14 = []
        let weights14 = []
        let result14 = []
        for (let i = 0; i < 5; i++) {
            input14.push(input[i])
            weights14.push(activWeights[i])
            result14.push(this.results.dataSync()[i])
        }
        for (let a = input.length - 5; a < input.length; a++) {
            input14.push(input[a])
            weights14.push(activWeights[a])
            result14.push(this.results.dataSync()[a])

        }

        input14.reverse()
        weights14.reverse()
        result14.reverse()
        // weights.forEach(el => el.dispose())
  

        return { input14, weights14, result14 }

    }

    gridTourGenerate(inputs, container, color) {
        let tensor = tf.tensor1d(inputs)
        let intensity = tensor.softmax().dataSync()
        const cage = document.createElement('table')
        let inputIndex = 0
        for (let i = 0; i < inputs.length; i++) {
            if (i === 5) {
                let tr = cage.insertRow(0)
                let td = tr.insertCell()
                const threeDots = document.createElement('img')
                threeDots.src = '3_dots.png'
                threeDots.style.width = '20px'
                td.style.borderColor = 'white'
                td.appendChild(threeDots)
            }
            let tr = cage.insertRow(0)
            let td = tr.insertCell()

            let valueDisplay = inputs[i].toFixed(2)
            td.innerHTML = valueDisplay
            const int = Math.round(intensity[i] * 1000) > 90 ? 90 : Math.round(intensity[i] * 1000)
            if (color == 22) td.style.borderColor = `hsl(${color} 100% ${int}%)`
            else td.style.borderColor = `hsl(${color} 0% ${int}%)`
            inputIndex++
tensor.dispose()
        }
        container.appendChild(cage)


    }





}

export default ReluExplained