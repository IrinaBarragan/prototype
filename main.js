import Interface from './srcThree/interface.js'
import Controller from "./src/controller.js"



const labelContainer = document.getElementById('prediction')
async function start() {
  
  const imageFromLocalStorage = localStorage.getItem("snapShotForVisualization")
  const image = document.getElementById('image-input')
  imageFromLocalStorage ? image.src = imageFromLocalStorage : image.src = 'aucuneImage.png'
    let metadataFromStorage = localStorage.getItem('modelMetadata')
    metadataFromStorage = JSON.parse(metadataFromStorage);
    let model
    if (metadataFromStorage) {
        model = await tmImage.load('indexeddb://vittascience-image-model', metadataFromStorage);
    }
    else {
        // for debugging we use local model from directory "public"
        model = await tmImage.load("modelVS/my-model.json", "modelVS/metadata.json");
    }
    const container = document.querySelector('#scene-container')


    for (let i = 0; i < model.getTotalClasses(); i++) { // and class labels
      labelContainer.appendChild(document.createElement('div'))
    }
    await predict(model, image)
    const interFace = new Interface(container, model, image)
    interFace.init()
    const controler = new Controller()
    controler.init(container)
}

async function predict (model, image) {
    // predict can take in an image, video or canvas html element
    const prediction = await model.predict(image)
    for (let i = 0; i < model.getTotalClasses(); i++) {
        const classPrediction =
          prediction[i].className + ': ' + prediction[i].probability.toFixed(2)
          document.querySelector('#prediction').childNodes[i].innerHTML = classPrediction
      }
  }
start()

