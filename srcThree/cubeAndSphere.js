import { EdgesGeometry, LineSegments, LineBasicMaterial, BoxBufferGeometry, Mesh, Matrix4, MeshStandardMaterial, SphereBufferGeometry, Group } from 'three'



class CubeAndSphere {
  constructor(width, length, zoom) {
    this.material = new MeshStandardMaterial({
      color: 'white'
    })
    this.material.color.convertSRGBToLinear();

    this.geometry = new BoxBufferGeometry(length * 0.1, width, width);

    // skew box
    const matrix = new Matrix4();
    matrix.makeShear(0, 0, 0, 0, 0.5, 0);
    this.geometry.applyMatrix4(matrix);



    // create a Mesh containing the geometry and material
    this.cube = new Mesh(this.geometry, this.material);
    this.cube.matrixAutoUpdate = false

    this.edges = new EdgesGeometry(this.geometry);
    this.edges.matrixAutoUpdate = false

    this.edgesMaterial = new LineBasicMaterial({
      color: '#59515E', linewidth: 2,
    });
    this.edgesMesh = new LineSegments(this.edges, this.edgesMaterial);

    this.group = new Group()
    this.group.add(this.cube)
    this.group.add(this.edgesMesh)
    this.group.rotation.set(Math.PI / 6, 0, 0);

    this.geometrySphere = new SphereBufferGeometry(width * zoom, 30, 20)
    this.sphere = new Mesh(this.geometrySphere, this.material);
    this.sphere.matrixAutoUpdate = false

  }

  getObjet() {

    return this.group
  }

  getSphere() {
    return this.sphere
  }


}

export default CubeAndSphere