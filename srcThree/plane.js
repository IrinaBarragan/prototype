import { Lut } from 'three/examples/jsm/math/Lut.js'
import { sRGBEncoding, PlaneBufferGeometry, MeshBasicMaterial, Mesh, PlaneGeometry, BoxHelper, DataTexture, RGBAFormat, UnsignedByteType, NormalBlending } from 'three'

// import * as THREE from 'three';


class Plane {
  constructor(maps, palette) {
    this.maps = maps
    this.arrayOfCards = []
    this.palette = 'cooltowarm'
    this.max = this.getMaxMin(this.maps).max
    this.min = this.getMaxMin(this.maps).min

  }

  getMaxMin(data) {
    let max = -1000
    let min =1000
    for (let i = 0; i < data.length; i++) {
      const dataPixel = tf.tidy(() => { return data[i].dataSync() })

      for (let j = 0; j < dataPixel.length; j++) {
        if (dataPixel[j] > max) {
          max = dataPixel[j]
        }
        if (dataPixel[j] < min) {
          min = dataPixel[j]
        }
      }
    }
    return { max, min }
  }

  cardRGB(shapeDataImage, maps, layer, numberOfCards, max, min, axis) {
    let cards
    for (let mapIndex = 0; mapIndex < numberOfCards; mapIndex++) {
      cards = this.createImageDataRGB(shapeDataImage, maps, mapIndex, max, min, axis)
      this.arrayOfCards.push(cards)
     
    }
    for (let n = 0; n < cards.length; n++) {
      cards[n].dispose()
    }
  }

  init(shape, shapeDataImage, numberOfCards, layer, x, y, z, axis) {

    this.geometry = new PlaneBufferGeometry(shape, shape)
    if (layer === 1) {
      this.cardRGB(shapeDataImage, this.maps, layer, numberOfCards, this.max, this.min, axis)
    } else {
      for (let mapIndex = 0; mapIndex < numberOfCards; mapIndex++) {
        const cards = this.createImageData(shapeDataImage, this.maps, mapIndex, axis)
        this.arrayOfCards.push(cards)
        for (let n = 0; n < cards.length; n++) {
          cards[n].dispose()
        }
      }
    }
    const box1 = this.cardsPositionVertical(shape, numberOfCards, layer, x, y, z)
    const box2 = this.cardsPosition2D(layer, x, y, z)
    return { box1, box2 }
  }

  initLayersShapeOne(shape, shapeDataImage, layer, x, y, z, axis) {
    //  first loop depend of filters number
    const max = this.getMaxMin(this.maps).max
    const min = this.getMaxMin(this.maps).min
    this.geometry = new PlaneBufferGeometry(shape, this.maps.shape[1])
    const cards = this.createImageDataLayerShapeOne(shapeDataImage, this.maps,  axis)
    cards.name = `${layer}_cardV_One`
    this.arrayOfCards.push(cards)
    const box1 = this.cardsPosition2D(layer, x, y, z)


    // tensor dispose

    for (let n = 0; n < cards.length; n++) {
      cards[n].dispose()
    }

    return { box1 }
  }

  cardsPositionVertical(shape, numberOfCards, layer, x, y, z) {
    const heightContainer = (shape+3) * numberOfCards
    const geometryBox = new  PlaneGeometry(shape + 2, heightContainer)
    const material = new  MeshBasicMaterial({ color: 'white' })
    material.color.convertSRGBToLinear();
    this.container = new  Mesh(geometryBox, material)
    this.container.position.set(-2.5, 0, z)
    const array = this.arrayOfCards
    for (let mapIndex = 0; mapIndex < array.length; mapIndex++) {
      this.container.add(array[mapIndex])
      array[mapIndex].position.set(x, (heightContainer / 2 - shape / 2) - (shape+3) * mapIndex , z)
      array[mapIndex].name = `${layer}_cardV_${mapIndex}`

      // outline
      this.container.add(new  BoxHelper(array[mapIndex], 'white'));

    }
    geometryBox.dispose()
    material.dispose()
    return this.container
  }

  cardsPosition2D(layer, x, y, z) {
    const map = this.arrayOfCards[0].clone()
    map.position.set(x - 2.5, 0, z)
    // map.name = `${layer}_card2D`
    return map
  }

  getMaxMinAroundZerro(max, min){
    const absMin = Math.abs(min)
    const absMax = Math.abs(max)
    max = absMax > absMin ? absMax : absMin
    min = -max
    return {max, min}
  }


  createImageData(shape, maps, mapIndex, axis) {
    if (this.palette == undefined) this.palette = 'cooltowarm'
    // this.getMaxMinAroundZerro(max, min)
    const max = this.getMaxMinAroundZerro(this.max, this.min).max
    const min = this.getMaxMinAroundZerro(this.max, this.min).min

    const lut = new Lut(this.palette, 512);
    let dataPixel
    let size = shape * shape
    if(axis == 0) {
      dataPixel = tf.tidy(() => { return maps[mapIndex].reverse(0).dataSync()})
    }
    else 
    dataPixel = tf.tidy(() => { return tf.reverse(maps[mapIndex]).dataSync()})

    //   dataImage
    const data = new Uint8Array(4 * size);
    for (let i = 0; i < size; i++) {

      let imgNormalize = (dataPixel[i] - min * 0.7) / (max * 0.7 - min * 0.7)

      const color = lut.getColor(imgNormalize)
      let stride = i * 4
      data[stride] = color.r * 255// red, and also X
      data[stride + 1] = color.g * 255// gree, and also Y
      data[stride + 2] = color.b * 255// blue
      data[stride + 3] = 255; // opacité
      // }

    }
    const dataTexture = new  DataTexture(data, shape, shape,  RGBAFormat,  UnsignedByteType);
    // dataTexture.encoding = sRGBEncoding;
    dataTexture.needsUpdate = true;
    const material = new  MeshBasicMaterial({
      map: dataTexture,
      blending:  NormalBlending, 
    })
    // material.color.convertSRGBToLinear();

    const card = new  Mesh(this.geometry, material);

    dataTexture.dispose()
    this.geometry.dispose()
    material.dispose()
    return card
  }

  createImageDataLayerShapeOne(shape, maps, axis) {

let dataPixel
    const lut = new Lut(this.palette, 512);
    const width = shape
    const height = maps.shape[1]
    const size = width * height
    if(axis == 0) {
      dataPixel = tf.tidy(() => { return maps.reverse(0).dataSync()})
    }
    else 
    dataPixel = tf.tidy(() => { return tf.reverse2d(maps).dataSync() })
    // min/max
    let maxDataMap = 0

    for (let i = 0; i < dataPixel.length; i++) {
      if (dataPixel[i] > maxDataMap) maxDataMap = dataPixel[i]

    }
    let minDataMap = -maxDataMap
    // const max = this.getMaxMinAroundZerro(maxDataMap, minDataMap).max
    // const min = this.getMaxMinAroundZerro(maxDataMap, minDataMap).min

    //   dataImage
    const data = new Uint8Array(4 * size);

    for (let i = 0; i < size; i++) {
      let imgNormalize = (dataPixel[i] - minDataMap) / (maxDataMap - minDataMap)
      // const imgNormalize = (dataPixel[i] - min) / (max - min)

      const color = lut.getColor(imgNormalize)
      let stride = i * 4
      data[stride] = color.r * 255// red, and also X
      data[stride + 1] = color.g * 255// gree, and also Y
      data[stride + 2] = color.b * 255// blue
      data[stride + 3] = 255; // opacité

    }

    const dataTexture = new  DataTexture(data, width, height,  RGBAFormat,  UnsignedByteType);
    // dataTexture.encoding = sRGBEncoding;
    dataTexture.needsUpdate = true;
    const material = new  MeshBasicMaterial({
      map: dataTexture, blending:  NormalBlending
    })

    const card = new  Mesh(this.geometry, material);

    return card
  }


  createImageDataRGB(shape, maps, mapIndex, max, min, axis) {

    let dataPixel
    if(axis == 0) {
      dataPixel = tf.tidy(() => { return maps[mapIndex].reverse(0).dataSync()})
    }
    else 
    dataPixel = tf.tidy(() => { return tf.reverse(maps[mapIndex]).dataSync()})
    const size = shape * shape
    const data = new Uint8Array(4 * size);

    for (let i = 0; i < size; i++) {
      let imgNormalize = (dataPixel[i] - min) / (max - min)

      if (mapIndex === 0) {

        let stride = i * 4
        data[stride] = imgNormalize * 255// red, and also X
        data[stride + 1] = 0
        data[stride + 2] = 0
        data[stride + 3] = 255; // opacité
      }

      if (mapIndex === 1) {

        let stride = i * 4
        data[stride] = 0
        data[stride + 1] = imgNormalize * 255
        data[stride + 2] = 0
        data[stride + 3] = 255; // opacité
      }

      if (mapIndex === 2) {

        let stride = i * 4
        data[stride] = 0
        data[stride + 1] = 0
        data[stride + 2] = imgNormalize * 255
        data[stride + 3] = 255; // opacité
      }


    }

    const dataTexture = new  DataTexture(data, shape, shape,  RGBAFormat,  UnsignedByteType);
    dataTexture.needsUpdate = true;
    const material = new  MeshBasicMaterial({
      map: dataTexture

    })

    const card = new  Mesh(this.geometry, material);
    dataTexture.dispose()
    this.geometry.dispose()
    material.dispose()

    return card
  }
}


export default Plane;
