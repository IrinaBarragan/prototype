import Camera from './camera.js'
import PlaneObject from './planeObject.js'
import { Renderer } from './renderer.js'
import Controller from '../src/controller.js'
import ImageOutline from './edge.js'
import Circle from './circle.js'

import { Vector2, Vector3, Raycaster, Group, Scene, Color, HemisphereLight, MeshBasicMaterial, Mesh } from 'three'
import Links from "./liens.js"

import CreateLayerOfModel from './createLayerOfModel.js'
import ZoomMouseControls from '../src/controlsZoom.js'
import { TrackballControls } from '../src/trackBallControl.js'
import TWEEN from '@tweenjs/tween.js'
import * as modifTHML from '../src/modifElementHTML.js'
import ConvolutionExplained from '../src/convolutionExplained.js'
import PointConvolutionExplained from '../src/pointConvolutionExplained.js'
import DeptWiseConvExplained from '../src/deptWiseConvolutionExplained.js'
import GapExplained from '../src/gapExplained.js'
import ReluExplained from '../src/reluExplained.js'
import SoftMaxExplained from '../src/softMaxExplained.js'





class CnnScene {

    constructor(container, activationCards, image, imgFlip, weights, labels, bias, prediction, axis) {

        this.container = container
        this.activationCards = activationCards
        this.weights = weights
        this.bias = bias
        this.prediction = prediction
        this.controller = new Controller()
        this.cnn = new ConvolutionExplained()
        this.pCnn = new PointConvolutionExplained()
        this.dwCnn = new DeptWiseConvExplained()
        this.gap = new GapExplained()
        this.reluExp = new ReluExplained()
        this.softmaxExp = new SoftMaxExplained()
        this.image = image
        this.imgFlip = imgFlip
        this.axis = axis
        this.buttonCameraOpen

        // parametres
        this.zoomSpeed = 1.3
        this.linkColor = "#aeaeae"


        this.scene = new Scene();
        this.scene.background = new Color('white');
        this.scene.background.convertSRGBToLinear();
        this.scene.position.set(0, 0, 0)
        this.scene.overrideMaterial

        // camera
        this.camera = new Camera(this.container)
        this.camera.position.set(0, 0, 1000);
        this.camera.lookAt(this.scene.position);
        // this.controlsCamera = true
        this.scene.add(this.camera)

        // renderer
        this.renderer = new Renderer(this.container)
        this.container.append(this.renderer.domElement);
        this.renderer.domElement.setAttribute("id", "canvasThreeJs")

        // trackBallControl
        this.controls = new TrackballControls(this.camera, this.renderer.domElement);
        this.controls.mouseButtons = { LEFT: 2, MIDDLE: 1, RIGHT: 0 }

        // light
        this.lightAmbient = new HemisphereLight(0xffffff, 0x080820, 3.5)
        this.lightAmbient.position.set(-150, 20, 250);
        this.scene.add(this.lightAmbient)

        // this.raycaster = new Raycaster();

        this.sceneContainer = this.createSceneContainer()

        this.zoomMouseControls = new ZoomMouseControls(this.camera, this.sceneContainer, this.controls);
        this.layer = new CreateLayerOfModel(this.container, activationCards, this.scene, this.camera, labels)
        this.imageOutlines = []
        this.linksRGB = []
        this.linksGAP = []
        this.links157 = []
        this.links156 = []
        this.linksConvolution = []
        this.addAllLayers(this.axis)
        this.cardsActivationDisplayed = false
        this.lienVisible = false
        this.frozen = false
        this.tick()

    }

    render() {
        this.renderer.render(this.scene, this.camera)
    }

    remove() {
        this.controls.dispose()
        this.renderer.dispose()
        this.render()
    }

    getCameraParameters(camera, controls) {
        const left = camera.left
        const right = camera.right
        const bottom = camera.bottom
        const top = camera.top
        const zoom = camera.zoom
        const cameraPosition = new Vector3(0, 0, 0)
        cameraPosition.set(camera.position.x, camera.position.y, camera.position.z)
        const targetPosition = new Vector3(0, 0, 0)
        targetPosition.set(controls.target.x, controls.target.y, controls.target.z)
        return { left, right, bottom, top, zoom, cameraPosition, targetPosition }
    }

    setCameraToInitialParameters() {
        this.camera.left = this.initialCameraParameters.left
        this.camera.right = this.initialCameraParameters.right
        this.camera.bottom = this.initialCameraParameters.bottom
        this.camera.top = this.initialCameraParameters.top
        this.camera.zoom = this.initialCameraParameters.zoom
        this.controls.target.set(this.initialCameraParameters.targetPosition.x, this.initialCameraParameters.targetPosition.y, this.initialCameraParameters.targetPosition.z)
        this.camera.position.set(this.initialCameraParameters.cameraPosition.x, this.initialCameraParameters.cameraPosition.y, this.initialCameraParameters.cameraPosition.z)
        this._resize()
        this.camera.updateProjectionMatrix()
    }

    createSceneContainer() {
        const box = new PlaneObject(this.container.clientWidth, this.container.clientHeight * 2, "white")
        box.material.opacity = '0'
        const sceneContainer = box.getObjet3D()
        sceneContainer.position.set(0, 0, 0)
        sceneContainer.name = 'sceneContainer'
        this.scene.add(sceneContainer)
        return sceneContainer
    }

    createBoxInfo(widthOfLayer) {
        let w = widthOfLayer
        let h = 100
        const box = new PlaneObject(w, h, "white")
        let boxInfo = box.getObjet3D()

        boxInfo.position.set(0, -w, 20)
        boxInfo.material.transparent = true
        boxInfo.material.opacity = 0
        boxInfo.name = 'boxInfo'
        return boxInfo
    }
    createBoxInfoBackground() {
        const widthI = Math.abs(this.camera.right) + Math.abs(this.camera.left)
        const box = new PlaneObject(widthI - 50, 160, "white")
        const createBoxInfoBackground = box.getObjet3D()
        let depth = 0.1
        let radius = 10
        let smoothness = 2
        createBoxInfoBackground.name = "boxInfoBackground"
        createBoxInfoBackground.visible = true
        createBoxInfoBackground.geometry = box.createBoxWithRoundedEdges(widthI - 50, 160, depth, radius, smoothness)
        createBoxInfoBackground.material.transparent = true
        createBoxInfoBackground.material.opacity = 1

        return createBoxInfoBackground
    }
    buttonInformation(widthOfLayer, txt) {
        const circleForTitle = new Circle(widthOfLayer * 0.11, '#cecece')
        circleForTitle.txt_mat.color.set("white")
        circleForTitle.txt_mat.color.convertSRGBToLinear();
        circleForTitle.addPredict(txt, -5, widthOfLayer * 0.28)
        const button = circleForTitle.getCapsuleThreeJs()
        return button
    }

    addAllLayers() {
        this.hideAllModals()
        const font = { small: 10, middle: 12, big: 14 }
        const y = 20

        this.boxImageInput = this.layer.initBox(this.imgFlip, y)
        const widthOfLayer = this.layer.getWidthOfLayer()
        const boxInfo = this.createBoxInfo(widthOfLayer)
        boxInfo.position.z = 20
        this.boxImageInput.add(boxInfo)
        this.sceneContainer.add(this.boxImageInput)


        let layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.inputLayer') : 'Couche d\'entrée'
        boxInfo.add(this.layer.addPanelInfo(layerTrans, font.big, 1))
        boxInfo.add(this.layer.addPanelInfo(' ', font.small, 1))

        boxInfo.add(this.layer.addPanelInfo('1 image', font.small, 1))
        boxInfo.add(this.layer.addPanelInfo('224×224 pixels', font.small, 1))

        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.openInfoBlock') : `ouvrir l'information`
        const buttonOpenInfo = this.buttonInformation(widthOfLayer, layerTrans)
        buttonOpenInfo.position.set(0, this.boxImageInput.position.y - 165)
        buttonOpenInfo.name = 'information'
        buttonOpenInfo.visible = false
        this.boxImageInput.add(buttonOpenInfo)

        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.closeInfoBlock') : `fermer l'information`
        const closeComposantInfo = this.buttonInformation(widthOfLayer, layerTrans)
        closeComposantInfo.name = 'closeComposantInfo'
        closeComposantInfo.position.set(0, boxInfo.position.y - 130)
        closeComposantInfo.visible = true
        this.boxImageInput.add(closeComposantInfo)
        // RGBlayer


        const layer1 = this.layer.init(1, '#D3D3D3', y, this.axis)
        let info = boxInfo.clone()
        layer1.add(info)
        this.sceneContainer.add(layer1)
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerRGB') : `Couche RGB`

        info.add(this.layer.addPanelInfo(layerTrans, font.big, 1))
        info.add(this.layer.addPanelInfo(' ', font.small, 1))
        info.add(this.layer.addPanelInfo('3 images', font.small, 1))
        info.add(this.layer.addPanelInfo('224×224 pixels', font.small, 1))
      


        // 2Layer (112x112)
        const layer2 = this.layer.init(2, 'white', y - 32, this.axis)
        const info2 = boxInfo.clone()
        layer2.add(info2)
        info2.add(this.layer.addPanelInfo('16 images', font.small))
        info2.add(this.layer.addPanelInfo('112×112 pixels', font.small))
        this.sceneContainer.add(layer2)

        // 3&4layers (56x56)

        const layer14 = this.layer.init(14, '#D3D3D3', y - 32, this.axis)
        const layer22 = this.layer.init(22, '#D3D3D3', y - 32, this.axis)
        const info3 = boxInfo.clone()
        layer14.add(info3)
        info3.position.x = this.layer.getWidthOfLayer() / 2
        info3.add(this.layer.addPanelInfo('48 images', font.small, 2))
        info3.add(this.layer.addPanelInfo('56×56 pixels', font.small, 2))

        this.sceneContainer.add(layer14)
        this.sceneContainer.add(layer22)

        // 32 layer (28x28)
        const layer32 = this.layer.init(32, 'white', y - 32, this.axis)
        const info4 = boxInfo.clone()
        layer32.add(info4)
        this.sceneContainer.add(layer32)
        info4.add(this.layer.addPanelInfo('48 images', font.small, 1))
        info4.add(this.layer.addPanelInfo('28×28 pixels', font.small, 1))

        // 40&49 layers(28x28)
        const layer40 = this.layer.init(40, '#D3D3D3', y - 32, this.axis)
        const layer49 = this.layer.init(49, '#D3D3D3', y - 32, this.axis)
        const info5 = boxInfo.clone()
        layer40.add(info5)
        this.layer.getWidthOfLayer()
        info5.position.x = this.layer.getWidthOfLayer() / 2
        info5.add(this.layer.addPanelInfo('96 images', font.small, 2))
        info5.add(this.layer.addPanelInfo('28×28 pixels', font.small, 2))
        this.sceneContainer.add(layer40)
        this.sceneContainer.add(layer49)

        // 59layer (14x14)
        const layer59 = this.layer.init(59, 'white', y, this.axis)
        const info6 = boxInfo.clone()
        layer59.add(info6)
        this.sceneContainer.add(layer59)
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layersConv') : `Couches de convolution`

        info6.add(this.layer.addPanelInfo(layerTrans, font.big + 5, 1))
        info6.add(this.layer.addPanelInfo(' ', font.small, 1))
        info6.add(this.layer.addPanelInfo('96 images', font.small, 1))
        info6.add(this.layer.addPanelInfo('14×14 pixels', font.small, 1))
        info6.add(this.layer.addPanelInfo(' ', font.big, 1))
        info6.add(this.layer.addPanelInfo(' ', font.big, 1))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.infoWindowFooter') : 'Ce réseau contient plus de 500 000 neurones. C\'est beaucoup, oui, mais moins que notre cerveau qui en contient 100 000 000 000 000!'
        info6.add(this.layer.addPanelInfo(layerTrans, font.big, 1))


        // 67&76&85&94layer (14x14) 
        const layer67 = this.layer.init(67, '#D3D3D3', y - 32, this.axis)
        const layer76 = this.layer.init(76, '#D3D3D3', y - 32, this.axis)
        const layer85 = this.layer.init(85, '#D3D3D3', y - 32, this.axis)
        const layer94 = this.layer.init(94, '#D3D3D3', y - 32, this.axis)
        const info7 = boxInfo.clone()
        layer76.add(info7)
        this.sceneContainer.add(layer67)
        this.sceneContainer.add(layer76)  // 67?
        info7.add(this.layer.addPanelInfo('144 images', font.small, 4))
        info7.add(this.layer.addPanelInfo('14×14 pixels', font.small, 4))
        this.sceneContainer.add(layer85)
        this.sceneContainer.add(layer94)
        info7.position.x = this.layer.getWidthOfLayer() / 2

        // 102&111 layer (14x14)
        const layer102 = this.layer.init(102, 'white', y - 32, this.axis)
        const layer111 = this.layer.init(111, 'white', y - 32, this.axis)
        const info8 = boxInfo.clone()
        layer102.add(info8)
        info8.add(this.layer.addPanelInfo('192 images', font.small, 2))
        info8.add(this.layer.addPanelInfo('14×14 pixels', font.small, 2))
        this.sceneContainer.add(layer102)
        this.sceneContainer.add(layer111)

        // 121 layer (7x7)
        const layer121 = this.layer.init(121, '#D3D3D3', y - 32, this.axis)
        this.sceneContainer.add(layer121)
        const info9 = boxInfo.clone()
        layer121.add(info9)
        info9.add(this.layer.addPanelInfo('192 images', font.small))
        info9.add(this.layer.addPanelInfo('7×7 pixels', font.small))

        // 129&138&147layer (7x7)
        const layer129 = this.layer.init(129, 'white', y - 32, this.axis)
        this.sceneContainer.add(layer129)
        const layer138 = this.layer.init(138, 'white', y - 32, this.axis)
        this.sceneContainer.add(layer138)
        const layer147 = this.layer.init(147, 'white', y - 32, this.axis)
        this.sceneContainer.add(layer147)
        const info10 = boxInfo.clone()
        layer138.add(info10)
        info10.add(this.layer.addPanelInfo('336 images', font.small, 3))
        info10.add(this.layer.addPanelInfo('7×7 pixels', font.small, 3))

        // 153layer(7x7)
        const layer153 = this.layer.init(154, '#D3D3D3', y - 32, this.axis)
        this.sceneContainer.add(layer153) //1280x7

        const info11 = boxInfo.clone()
        layer153.add(info11)
        info11.add(this.layer.addPanelInfo('1280 images', font.small))
        info11.add(this.layer.addPanelInfo('7×7 pixels', font.small))

        this.layerStart153 = this.layer.groupOfPlanes2D
        this.activMapsStart153 = this.getActivationMap(155)
        // #d2ece1 green #303030 gray

        // gapLayer
        const layer155 = this.layer.initShapeEqualOne(155, '#d2ece1', y, this.axis)
        const info12 = boxInfo.clone()
        layer155.add(info12)
        this.sceneContainer.add(layer155)  //GAP
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerAgreg.firstWord') : `Couche`

        info12.add(this.layer.addPanelInfo(layerTrans, font.big))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerAgreg.secondWord') : `d'agrégation `

        info12.add(this.layer.addPanelInfo(layerTrans, font.big))
        info12.add(this.layer.addPanelInfo('1280 données', font.small))
        info12.add(this.layer.addPanelInfo('1x1 pixels', font.small))

        this.layerStart155 = this.layer.groupOfPlanes2D
        this.activMapsStart155 = this.getActivationMap(155)

        // DenseLayer
        const layer156 = this.layer.initShapeEqualOne(156, '#white', y)
        const info13 = boxInfo.clone()
        layer156.add(info13)
        this.sceneContainer.add(layer156) //Dense1  1x100
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerDense.firstWord') : `Couche`

        info13.add(this.layer.addPanelInfo(layerTrans, font.big))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerDense.secondWord') : `dense `

        info13.add(this.layer.addPanelInfo(layerTrans, font.big))
        info13.add(this.layer.addPanelInfo('100 données', font.small))
        info13.add(this.layer.addPanelInfo('1×1 pixels', font.small))

        this.layerStart156 = this.layer.groupOfPlanes2D
        this.activMapsStart156 = this.getActivationMap(156)


        const layer157 = this.layer.initShapeEqualOne(157, '#white', y)
        this.sceneContainer.add(layer157)  //Dense2 1xn numberOf Category
        const info14 = boxInfo.clone()
        layer157.add(info14)
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerDense.firstWord') : `Couche`

        info14.add(this.layer.addPanelInfo(layerTrans, font.big))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.layerDense.secondWord') : `de sortie`

        info14.add(this.layer.addPanelInfo(layerTrans, font.big))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.prediction.firstWord') : `Couche`

        info14.add(this.layer.addPanelInfo(layerTrans, font.small))
        layerTrans = typeof i18next !== 'undefined' ? i18next.t('code.aiVisualization.threeJS.prediction.secondWord') : `du modèle`

        info14.add(this.layer.addPanelInfo(layerTrans, font.small))

        info14.children.forEach(el => el.position.x = 20)
        info14.position.x = 20

        const lengthUsed = this.layer.getLengthUsed()
        this.resetCamera(lengthUsed)
        this.layer.arrayInfo.push(boxInfo, info, info2, info3, info4, info5, info6, info7, info8, info9, info10, info11, info12, info13, info14)
        this.layer.arrayInfo.forEach(el => el.visible = true)

        const createBoxInfoBackground = this.createBoxInfoBackground()
        createBoxInfoBackground.position.set(-50, info.position.y-20, 460)
        const outlineMaterial = new MeshBasicMaterial( { color: 0x999999 } );
		const outlineMesh = new Mesh(createBoxInfoBackground.geometry, outlineMaterial );
        outlineMesh.name = "outlineMesh"
        outlineMesh.position.x=createBoxInfoBackground.position.x
        outlineMesh.position.y=createBoxInfoBackground.position.y
        outlineMesh.position.z=createBoxInfoBackground.position.z-1
		outlineMesh.scale.set( 1.001, 1.01 );
        layer76.add(createBoxInfoBackground)
		layer76.add(outlineMesh);
        this.initialCameraParameters = this.getCameraParameters(this.camera, this.controls)

        this.addLinksInScene()

    }
    addLinksInScene() {
        this.createLinksRGB()
        for (let i = 1; i <= 18; i++) {
            this.createEdgeAndLink(i, 3, "white", this.linkColor)
        }
        this.createLinksGAP()
        this.createLinksDenseSoftMax()
        this.createLinksDenseRelu()
    }
    resetCamera(lengthUsed) {
        // const right = lengthUsed - this.container.clientWidth + 120
        const left = lengthUsed - this.container.clientWidth + 120
        const h = Math.abs(this.camera.top) + Math.abs(this.camera.bottom)
        const w = Math.abs(this.camera.right) + Math.abs(this.camera.left)
        const coeff = h / w
        this.camera.right += left
        // this.camera.left -=w* coeff / 2
        this.camera.top += left * coeff / 2
        this.camera.bottom -= left * coeff / 2
        this.camera.updateProjectionMatrix();
    }


    createEdgeAndLink(layer, edgeSize = 3, colorEdge, colorLiens) {
        const start = this.layer.getArrayLayers2Dd()[layer - 1]
        const finish = this.layer.getArrayLayers2Dd()[layer]

        let card
        const group = new Group()
        let arrayLiens = []

        if (start.children[0].geometry.parameters.height === finish.children[0].geometry.parameters.height) {
            arrayLiens.push(1)
        } else {
            arrayLiens.push(2)
        }
        if (start) {
            card = start.children.find(element => element.name.includes('cardV_0'))
            const interval = card.geometry.parameters.height + 3
            let positionStart = card.getWorldPosition(new Vector3(0, 0, 0))

            const edge = new ImageOutline(edgeSize, edgeSize, colorEdge)
            edge.material.transparent = true
            edge.material.opacity = 0.2


            const linkGray = new Links(colorLiens, .5)
            let count = 0
            for (let i = 0; i < start.geometry.parameters.height; i += interval) {
                const startX = positionStart.x - card.geometry.parameters.height / 2 + 1.5
                const startY = positionStart.y - card.geometry.parameters.height / 2 + 1.5

                const edgeLine = edge.getObjet3D(count)
                count += 1
                edgeLine.visible = false
                edgeLine.name = `${i}_edge`
                edgeLine.position.set(startX, startY - i, positionStart.z + 10)
                group.add(edgeLine)

                group.name = `groupOfEdge_${layer}`
                this.sceneContainer.add(group)

                const lien = linkGray.getLien(
                    new Vector3(startX + 1.5, startY - 1.5 - i, positionStart.z),
                    new Vector3(startX + 20, startY - i, positionStart.z))
                lien.name = `lien`
                lien.visible = false
                this.sceneContainer.add(lien)
                arrayLiens.push(lien)
            }
        }
        let array = []
        array.push(group)
        array.push(arrayLiens)
        this.imageOutlines.push(array)
        arrayLiens = []
        array = []
    }

    createLinksDenseSoftMax() {
        const layerStart = this.layer.getArrayLayers2Dd()[20]
        const positionStart = layerStart.getWorldPosition(new Vector3(0, 0, 0))
        const pointStart = new Vector3(positionStart.x, layerStart.geometry.parameters.height / 2 - .5, positionStart.z)
        const linkGray = new Links(this.linkColor, .2)
        linkGray.material.color.convertSRGBToLinear();
        for (let i = 0; i < layerStart.geometry.parameters.height; i++) {
            const lien = linkGray.getLien(
                new Vector3(pointStart.x, pointStart.y - i, pointStart.z),
                new Vector3(pointStart.x + 1, pointStart.y - i, pointStart.z))
            this.sceneContainer.add(lien)
            this.links157.push(lien)
        }
        this.links157.forEach(el => {
            el.visible = false
        })
    }
    updateLinksDenseSoftMax(row, x, y) {
        const layerStart = this.links157
        layerStart.forEach(el => {
            el.visible = true
        })
        let mousePosition = new Vector3(x, y, 0.5)
            .unproject(this.camera);

        const activMapsStart = this.getActivationMap(156).dataSync()
        const activMapsEnd = this.getActivationMap(157).dataSync()

        let index
        if (activMapsEnd.length % 2 == 0) {
            index = row + activMapsEnd.length / 2
        } else {
            index = (Math.floor(row + activMapsEnd.length / 2 - 0.5))
        }
        const dataLiens = this.getWeigths(157).map(el => ((el.mul(activMapsStart))))
        const weightslayer = dataLiens[activMapsEnd.length - index - 1].dataSync()
        this.softMax(dataLiens, activMapsEnd.length - index - 1)
        for (let i = 0; i < layerStart.length; i++) {
            layerStart[i].visible = true
            layerStart[i].geometry.attributes.position.array[3] = mousePosition.x
            layerStart[i].geometry.attributes.position.array[4] = mousePosition.y
            layerStart[i].geometry.attributes.position.needsUpdate = true;

            const dataLinks = weightslayer[i]
            if (dataLinks <= 0) {
                layerStart[i].visible = false
            }

            if (dataLinks > 0.2) {
                layerStart[i].material.color.set('green')

            }
            if (dataLinks > 0.5) {
                layerStart[i].material.color.set('red')
            }

        }

    }

    softMax(dataLinks, index) {
        const results = dataLinks.map(el => el.sum().dataSync()[0])
        const tensor = tf.tensor1d(results)
        const softMaxTensor = tensor.softmax()
    }

    createLinksDenseRelu() {
        const layerStart = this.layer.getArrayLayers2Dd()[19]
        const positionStart = layerStart.getWorldPosition(new Vector3(0, 0, 0))
        const pointStart = new Vector3(positionStart.x, layerStart.geometry.parameters.height / 2 - .5, positionStart.z)

        const linkGray = new Links(this.linkColor, .2)
        // linkGray.material.color.convertSRGBToLinear();
        for (let i = 0; i < layerStart.geometry.parameters.height; i++) {
            const lien = linkGray.getLien(
                new Vector3(pointStart.x, pointStart.y - i, pointStart.z),
                new Vector3(pointStart.x + 1, pointStart.y - i, pointStart.z))
            this.sceneContainer.add(lien)
            this.links156.push(lien)
        }
        this.links156.forEach(el => {
            el.visible = false
        })

    }
    updateLinksDenseRelu(row, x, y) {
        const layerStart = this.links156
        layerStart.forEach(el => {
            el.visible = true
        })
        let mousePosition = new Vector3(x, y, 0.5)
            .unproject(this.camera);

        const activMapsStart = this.getActivationMap(155).dataSync()

        const activMapsEnd = this.getActivationMap(156).dataSync()
        const index = row + activMapsEnd.length / 2

        const dataLiens = this.getWeigths(156).map(el => ((el.mul(activMapsStart))))
        const weightslayer = dataLiens[activMapsEnd.length - index - 1].dataSync()


        for (let i = 0; i < layerStart.length; i++) {
            layerStart[i].visible = true
            layerStart[i].geometry.attributes.position.array[3] = mousePosition.x
            layerStart[i].geometry.attributes.position.array[4] = mousePosition.y
            layerStart[i].geometry.attributes.position.needsUpdate = true;

            const dataLinks = weightslayer[i]

            if (dataLinks <= 0) {
                layerStart[i].visible = false

            }
            if (dataLinks > 0.1) {
                layerStart[i].material.color.set('green')
                layerStart[i].material.linewidth = 2
            }
            if (dataLinks > 0.2) {
                layerStart[i].material.color.set('red')
                layerStart[i].material.linewidth = 2

            }

        }

    }
    createLinksRGB() {
        const startLayer = this.boxImageInput
        const linkGray = new Links("gray", 1.7)

        const start = startLayer.getWorldPosition(new Vector3(0, 0, 0))

        const pointStart = new Vector3(start.x, start.y, start.z)
        const lienBlanc = linkGray.getLien(
            new Vector3(pointStart.x, pointStart.y, pointStart.z + 500),
            new Vector3(pointStart.x, pointStart.y, pointStart.z + 500))
        lienBlanc.name = "lien_RGB"
        const lien = new Links("white", 1)
        const lienNoir = lien.getLien(
            new Vector3(pointStart.x + .5, pointStart.y, pointStart.z + 500),
            new Vector3(pointStart.x + .5, pointStart.y, pointStart.z + 500))
        lienNoir.name = "lien_RGB"
        this.sceneContainer.add(lienBlanc)
        this.sceneContainer.add(lienNoir)

        this.linksRGB.push(lienBlanc)
        this.linksRGB.push(lienNoir)

    }

    updateLinksRGB(x, y) {
        const position = this.mousePositionLocal(x, y)
        this.linksRGB.forEach(el => el.visible = true)
        let mousePosition = new Vector3(x, y, 0)
            .unproject(this.camera);

        const start = this.boxImageInput.getWorldPosition(new Vector3(0, 0, 0))

        const pointStart = new Vector3(start.x, start.y, start.z)
        const widthBox = this.boxImageInput.geometry.parameters.width
        this.linksRGB.forEach(el => {
            el.geometry.attributes.position.array[0] = pointStart.x - widthBox / 2 + position.col
            el.geometry.attributes.position.array[1] = pointStart.y - widthBox / 2 + position.row
            el.geometry.attributes.position.array[3] = mousePosition.x
            el.geometry.attributes.position.array[4] = mousePosition.y
            el.geometry.attributes.position.needsUpdate = true;
        })
    }

    createLinksGAP() {
        const startLayer = this.layer.getArrayLayers2Dd()[18]
        const finishLayer = this.layer.getArrayLayers2Dd()[19]

        const linkGray = new Links(this.linkColor, .2)
        linkGray.material.color.convertSRGBToLinear();
        const start = startLayer.getWorldPosition(new Vector3(0, 0, 0))
        const finish = finishLayer.getWorldPosition(new Vector3(0, 0, 0))

        const pointStart = new Vector3(start.x, start.y, start.z)
        const pointFinish = new Vector3(finish.x, finish.y, finish.z)

        const lien = linkGray.getLien(
            new Vector3(pointStart.x, pointStart.y, pointStart.z + 10),
            new Vector3(pointFinish.x, pointFinish.y, pointFinish.z))
        lien.name = "lien_Gap"
        lien.visible = false
        this.sceneContainer.add(lien)
        this.linksGAP.push(lien)
    }

    updateLinksGAP(row, x, y) {

        // const linkGap = this.sceneContainer.children.find(el => el.name === "lien_Gap")
        const linkGap = this.linksGAP[0]
        linkGap.visible = true
        const index = 1280 - (row + 1280 / 2)
        const startLayer = this.layer.getArrayLayers2Dd()[18].children[index * 2]
        let mousePosition = new Vector3(x, y, 0.5)
            .unproject(this.camera);

        const start = startLayer.getWorldPosition(new Vector3(0, 0, 0))
        const pointStart = new Vector3(start.x, start.y, start.z)
        linkGap.geometry.attributes.position.array[0] = pointStart.x
        linkGap.geometry.attributes.position.array[1] = pointStart.y + 10
        linkGap.geometry.attributes.position.array[3] = mousePosition.x
        linkGap.geometry.attributes.position.array[4] = mousePosition.y
        linkGap.geometry.attributes.position.needsUpdate = true;
    }

    getWeigths(layer) {
        const weights = this.weights[layer]
        const weightslayer = tf.tidy(() => {
            return tf.unstack(weights[0].reshape([weights[0].shape[0], weights[0].shape[1]]), 1)
        })
        return weightslayer
    }
    

    getActivationMap(layer) {
        return tf.tidy(() => {
            if ((layer == 155) || (layer == 156) || (layer == 157)) {
                return this.activationCards[layer]
            } else {
                return tf.tidy(() => {
                    return tf.unstack(this.activationCards[layer].reshape([this.activationCards[layer].shape[1], this.activationCards[layer].shape[2], this.activationCards[layer].shape[3]]), 2)
                });
            }
        })
    }

    // zoomToPosition
    executeMouseWheelZooming(wheelDelta, x, y) {
        let zoomFactor = wheelDelta * 250;
        let position = new Vector2((x / this.container.offsetWidth) * 2 - 1,
            -(y / this.container.offsetHeight) * 2 + 1);
        this.zoomMouseControls.zoomToPosition(zoomFactor, position, this.camera, this.container);
    }
    executeMouseZoomingClick(wheelDelta, x, y) {
        let position = new Vector2((x / this.container.offsetWidth) * 2 - 1,
            -(y / this.container.offsetHeight) * 2 + 1);
        this.layer._zoomClick(this.zoomMouseControls, position, this.camera, this.container)
    }

    init(modelLayersName) {
        this.modelLayersName = modelLayersName
        this.controller.on("onMouseOver", "onMouseOver", () => {
            event.preventDefault();
            const x = event.offsetX / this.container.clientWidth * 2 - 1
            const y = -(event.offsetY / this.container.clientHeight) * 2 + 1
            this.onMouseOver(x, y, event)

        })
        this.controller.on("onMouseWheel", "wheel", (e) => {
            if ((Math.abs(this.camera.left) + Math.abs(this.camera.right) > 10000) && event.wheelDelta < 0) return
            const wheelDelta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail || event.deltaY)));
            this.executeMouseWheelZooming(wheelDelta, event.offsetX, event.offsetY);

        })

        this.controller.on("monMouseClick", "monMouseClick", () => {
            event.preventDefault();
            const x = event.offsetX / this.container.clientWidth * 2 - 1
            const y = -(event.offsetY / this.container.clientHeight) * 2 + 1
            this._mouseClick(event, x, y)
        })

        // dbClick
        this.controller.on("zoomClick", "zoomClick", () => {
            this._zoomClick(event)
        })
        this.controller.on('resize', "monResize", () => {
            this.setCameraToInitialParameters()
        })

    }

    _resize() {

        let aspect = this.container.clientHeight / this.container.clientWidth
        this.camera.top = this.camera.frustumSize * aspect
        this.camera.bottom = -this.camera.frustumSize * aspect * 1.2
        this.renderer.setSize(this.container.clientWidth, this.container.clientHeight);
        this.renderer.setPixelRatio(window.devicePixelRatio * 2, 2)
        this.camera.updateProjectionMatrix()

    }

    async _mouseClick(event, x, y) {
        let x0, y0
        let switchLinksFreeze = true
        this.container.addEventListener("mousedown", (e) => {
            x0 = e.clientX
            y0 = e.clientY
        })
        this.container.addEventListener("mouseup", (e) => {
            // comment here
            if ((x0 == e.clientX) && (y0 == e.clientY)) {
                switchLinksFreeze = true
            } else {
                switchLinksFreeze = false
            }
        })

        event.preventDefault()

        if (switchLinksFreeze === true) {
            const raycaster = this.monRayCaster(x, y)
            const intersects = raycaster.intersectObjects(this.scene.children);
            this.buttonCameraOpen =intersects.find(element => element.object.name.includes('buttonCameraOpen'))

        if ((this.buttonCameraOpen) && (this.buttonCameraOpen.object.visible)) {
            this.buttonCameraOpen.object.visible = false
  
                    document.querySelector("#imageBox").style.display = 'block'
                    document.querySelector('#realTemps').style.display = 'block'
                    document.querySelector("#videoImage").classList = 'btn btn-primary'
   
        }
            if (intersects.find(element => element.object.name.includes('_cardV_'))) {
                const intersect = intersects.find(element => element.object.name.includes('_cardV_'))
                const intersectIndex = intersect.object.name.split("_")[2]
                const layerIndex = intersect.object.name.split("_")[0]
                if (this.frozen === false) {
                    this.frozen = true

                    const position = this.mousePositionLocal(x, y)

                    const arrayLayers = ['14', '22', '32', '40', '49', '59', '67', '76', '85', '94', '102', '111', '121', '129', '138', '147']
                    if ((position) && (intersect.object.name.split("_")[0] == 2)) {

                        const weights = this.weights[position.layer]
                        const activCards = this.getActivationMap(position.layer - 1)
                        modifTHML.getCnnModal(weights, activCards, this.cnn, intersectIndex, position)
                    }

                    if ((position) && (arrayLayers.includes(layerIndex.toString()))) {
                        const weights = this.weights[position.layer]
                        const activCards = this.getActivationMap(position.layer - 1)
                        let indexPreviousLayer = arrayLayers.indexOf(position.layer) - 1

                        let index = arrayLayers[indexPreviousLayer]
                        if (indexPreviousLayer == -1) index = 2
                        const activPreviousLayer = this.getActivationMap(index)
                        const modelLayersName = this.modelLayersName.slice(parseInt(index) + 1, position.layer)

                        modifTHML.getDWCnnModal(weights, activCards, activPreviousLayer, this.dwCnn, intersectIndex, position, modelLayersName)


                    }
                    if ((position) && (intersect.object.name.split("_")[0] == 154)) {

                        const modelLayersName_one = this.modelLayersName.slice(148, 152)
                        const modelLayersName_two = this.modelLayersName.slice(153, 155)
                        const weights = this.weights[152]
                        const activCardsLayerPreviosly = this.getActivationMap(147)
                        const activCards = this.getActivationMap(151)
                        const activCardsForBN = this.getActivationMap(153)
                        const activCardsForRelu = this.getActivationMap(154)

                        modifTHML.getPointCnnModal(weights, activCards, activCardsLayerPreviosly, this.pCnn, intersectIndex, position, activCardsForBN, activCardsForRelu, modelLayersName_one, modelLayersName_two)

                    }
                    if ((position) && (intersect.object.name.split("_")[0] == 155)) {
                        tf.tidy(() => {
                            const activCards = this.getActivationMap(position.layer - 1)
                            // couche gap
                            const input = this.gap.getInputs(activCards, position)
                            this.gap.displayModal(input)
                        })
                    }
                    if ((position) && (intersect.object.name.split("_")[0] == 156)) {
                        tf.tidy(() => {
                            const activCards = this.getActivationMap(position.layer - 1)
                            // couche relu
                            const weights = this.getWeigths(position.layer)
                            const inputAndWeightsForDisplay = this.reluExp.getInputs(activCards, weights, position, this.bias)
                            const input = inputAndWeightsForDisplay.input14
                            const weight14 = inputAndWeightsForDisplay.weights14
                            const result = inputAndWeightsForDisplay.result14

                            this.reluExp.displayModal(input, weight14, result)
                            weights.forEach(el => el.dispose())
                        })
                    }
                    if ((position) && (position.layer == 157)) {
                        // couche sortie
                        tf.tidy(() => {
                            const activCards = this.getActivationMap(position.layer - 1)
                            const weights = this.getWeigths(position.layer)
                            const inputAndWeightsForDisplay = this.softmaxExp.getData(activCards, weights, position)

                            const input = inputAndWeightsForDisplay.input14
                            const weight = inputAndWeightsForDisplay.weights14
                            const result = inputAndWeightsForDisplay.result14
                            const allRes = inputAndWeightsForDisplay.allResults
                            this.softmaxExp.displayModal(input, weight, result, allRes, this.getActivationMap(position.layer), this.prediction)
                        })
                    }

                } else
                    this.frozen = false
                if (this.frozen === false) {
                    if (this.cnn.getModalState()) this.cnn.hideModal()
                    if (this.dwCnn.getModalState()) this.dwCnn.hideModal()
                    if (this.pCnn.getModalState()) this.pCnn.hideModal()
                    if (this.gap.getModalState()) this.gap.hideModal()
                    if (this.reluExp.getModalState()) this.reluExp.hideModal()
                    if (this.softmaxExp.getModalState()) this.softmaxExp.hideModal()
                }
            }
            if (this.cardsActivationDisplayed === false) {
                this.displayCardsActivation(true, true, false, false)
                modifTHML.switchButton3D2D(true, false)
            }
            this.layer.click(this.controls)

        } else {
            return
        }
    }

    hideAllModals() {
        this.cnn.hideModal()
        this.gap.hideModal()
        this.reluExp.hideModal()
        this.softmaxExp.hideModal()
    }

    displayCardsActivation(twoDd, buttonStop, threeDm, twoDm) {
        this.cardsActivationDisplayed === false ? this.cardsActivationDisplayed = true : this.cardsActivationDisplayed = false
        const result = this.returnTous()
        const arrayLayers2Dm = result.a2Dm
        const arrayLayers2Dd = result.a2Dd
        const arrayButtonStop = result.buttonStop
        const arrayLayers3Dm = result.a3Dm


        arrayLayers2Dd.forEach(element => {
            setTimeout(function () {
                element.visible = twoDd
                element.children.forEach(el => el.visible = twoDd)
            }, 500)

        });
        arrayButtonStop.forEach(element => {
            setTimeout(function () {
                element.visible = buttonStop
            }, 500)

        });

        arrayLayers3Dm.forEach(element => {
            if (threeDm === true) {
                setTimeout(function () {
                    element.rotation.set(Math.PI / 6, 0, 0)
                    element.visible = threeDm
                }, 500)

            } else {
                new TWEEN.Tween(element.rotation)
                    .to({ x: 0, y: Math.PI / 4, z: 0 }, 500)
                    .easing(TWEEN.Easing.Bounce.In)
                    .start()

                setTimeout(function () {
                    element.visible = threeDm
                }, 490)
            }
        });
        arrayLayers2Dm.forEach(element => {

            element.visible = twoDm
        });
        setTimeout(() => this.visibleOnOff(), 500)
    }

    monRayCaster(x, y) {
        const raycaster = new Raycaster();
        let mouse = new Vector2()
        mouse.x = x
        mouse.y = y
        raycaster.ray.origin.set(mouse.x, mouse.y, 800);
        this.camera.localToWorld(raycaster.ray.origin);
        raycaster.ray.origin.z = this.camera.far;
        raycaster.setFromCamera(mouse, this.camera);
        return raycaster
    }

    displayInfoBlock(infoBlockVisible) {
        if (infoBlockVisible) {
            document.querySelector('#dataValue').style.visibility = "visible"

        } else {
            document.querySelector('#dataValue').style.visibility = "hidden"
        }
    }

    onMouseOver(x, y) {
        dataValue.style.transform = `translate(${event.offsetX - this.container.clientWidth}px,${event.offsetY}px)`
        const indexOfPixel = this.dataDisplay(x, y)
        if (this.frozen === true) return
        if ((this.lienVisible) && (this.mousePositionLocal(x, y) != 'undefined')) {
            this.mousePositionLocal(x, y)
            if (this.mousePositionLocal(x, y)) {
                const pos = this.mousePositionLocal(x, y)
                const layer = pos.layer
                const row = pos.row
                const col = pos.col
                if (layer == 155) {
                    this.updateLinksGAP(row, x, y)
                    this.linksRGB.forEach(el => el.visible = false)
                    this.links156.forEach(el => el.visible = false)
                    this.links157.forEach(el => el.visible = false)
                    if (this.linksConvolution.length > 0) {

                        for (let i = 1; i < this.linksConvolution[1].length; i++) {
                            this.linksConvolution[1][i].visible = false
                        }
                    }

                } else if (layer == 156) {
                    this.updateLinksDenseRelu(row, x, y)
                    this.linksRGB.forEach(el => el.visible = false)
                    this.links157.forEach(el => el.visible = false)
                    this.linksGAP.forEach(el => el.visible = false)
                    if (this.linksConvolution.length > 0) {

                        for (let i = 1; i < this.linksConvolution[1].length; i++) {
                            this.linksConvolution[1][i].visible = false
                        }
                    }


                } else if (layer == 157) {
                    this.updateLinksDenseSoftMax(row, x, y)
                    this.linksRGB.forEach(el => el.visible = false)
                    this.links156.forEach(el => el.visible = false)
                    this.linksGAP.forEach(el => el.visible = false)
                    if (this.linksConvolution.length > 0) {

                        for (let i = 1; i < this.linksConvolution[1].length; i++) {
                            this.linksConvolution[1][i].visible = false
                        }
                    }
                } else if (layer == 1) {
                    this.updateLinksRGB(x, y)
                    this.linksRGB.forEach(el => el.visible = true)
                    this.links156.forEach(el => el.visible = false)
                    this.links157.forEach(el => el.visible = false)
                    this.linksGAP.forEach(el => el.visible = false)
                    if (this.linksConvolution.length > 0) {
                        this.linksConvolution[0].visible = false
                        for (let i = 1; i < this.linksConvolution[1].length; i++) {
                            this.linksConvolution[1][i].visible = false

                        }
                    }

                }
                else {
                    this.linksRGB.forEach(el => el.visible = false)
                    this.links156.forEach(el => el.visible = false)
                    this.links157.forEach(el => el.visible = false)
                    this.linksGAP.forEach(el => el.visible = false)
                    const arrayLayers2Dd = this.layer.getArrayLayers2Dd()
                    const index = arrayLayers2Dd.findIndex(element => (element.name.split('_')[0] === layer))
                    const antiIndex = arrayLayers2Dd.findIndex(element => (element.name.split('_')[0] != layer))
                    this.updateLinksConvolution(x, y, index, indexOfPixel, row, col, layer)
                    if (this.linksConvolution.length > 0) {
                        this.linksConvolution[0].visible = true

                        for (let i = 1; i < this.linksConvolution[1].length; i++) {
                            this.linksConvolution[1][i].visible = true
                        }
                    }
                }
            }
        } else {
            this.setVisible()
        }

    }

    linkInvisible() {
        this.linksRGB.forEach(el => el.visible = false)
        this.links156.forEach(el => el.visible = false)
        this.links157.forEach(el => el.visible = false)
        this.linksGAP.forEach(el => el.visible = false)
        const arrayLayers2Dd = this.layer.getArrayLayers2Dd()

        if (this.linksConvolution.length > 0) {
            this.linksConvolution[0].visible = false

            for (let i = 1; i < this.linksConvolution[1].length; i++) {
                this.linksConvolution[1][i].visible = false
            }
        }
    }

    updateLinksConvolution(x, y, index, indexOfPixel, row, col, layer) {

        const position = this.mousePositionLocal(x, y)
        if (this.imageOutlines.length > 0) {
            const groupEdge = this.imageOutlines[index - 1][0]

            const groupLinks = this.imageOutlines[index - 1][1]
            if (index != this.indexVisible || index == 'undefined') {
                this.setVisible(groupEdge, groupLinks)
                this.indexVisible = index
            }
            const myCase = groupLinks[0]
            groupEdge.position.set(+position.col * myCase, +position.row * myCase, groupEdge.position.z)
            for (let i = 1; i < groupLinks.length; i++) {
                let mousePosition = new Vector3(x, y, 0.5)
                    .unproject(this.camera);

                const xl = groupEdge.children[i - 1].position.x + position.col * myCase + 1.5
                const yl = groupEdge.children[i - 1].position.y + position.row * myCase - 1.5

                groupLinks[i].geometry.attributes.position.array[0] = xl
                groupLinks[i].geometry.attributes.position.array[1] = yl
                // groupLinks[i].geometry.attributes.position.array[2]=groupEdge.position.z
                groupLinks[i].geometry.attributes.position.array[3] = mousePosition.x
                groupLinks[i].geometry.attributes.position.array[4] = mousePosition.y
                // groupLinks[i].geometry.attributes.position.array[5]=groupEdge.position.z
                groupLinks[i].geometry.attributes.position.needsUpdate = true;
            }
        }
    }


    setVisible(edges, liens) {
        if (this.linksConvolution.length > 0) {
            this.linksConvolution[0].children.forEach(el => {
                el.visible = false
            });
            for (let i = 1; i < this.linksConvolution[1].length; i++) {
                this.linksConvolution[1][i].visible = false
            }
        }
        this.linksConvolution = []
        if (edges && liens) {
            this.linksConvolution.push(edges)
            this.linksConvolution.push(liens)

            edges.children.forEach(el => {
                el.visible = true
            });
            for (let i = 1; i < liens.length; i++) {
                liens[i].visible = true
            }
        }
    }

    mousePositionLocal(x, y) {
        let row, col
        const raycaster = this.monRayCaster(x, y)
        const intersects = raycaster.intersectObjects(this.scene.children)
        let layerSelected = intersects.find(element => element.object.name.includes('layer'))
        const intersectB = intersects.find(element => element.object.name.includes('_box'))
        const button = intersects.find(element => element.object.name.includes('buttonStop'))

        if (intersectB) layerSelected = intersectB

        let layer
        if (layerSelected) layer = layerSelected.object.name.split('_')[0]
        let intersect = intersects.find(element => element.object.name.includes('cardV'))
        let arrayLayers2D = this.layer.getArrayLayers2Dd()
        let index = arrayLayers2D.findIndex(element => element.name.split('_')[0] === this.layerSelected)
        if ((!intersect)) return
        else if ((index === 1) && (!arrayLayers2D[0].visible)) return
        else if ((index > 1) && ((!intersect.object.visible) || (!arrayLayers2D[index - 1].visible))) {
            return
        } else {
            this.lienVisible = true
        }

        let localPosition;
        if (intersect) {
            this.layerSelected = intersect.object.name.split('_')[0]
            const intersectIndex = intersect.object.name.split("_")[2]

            const worldPosition = intersect.point
            intersect.object.updateMatrixWorld()
            localPosition = intersect.object.worldToLocal(worldPosition)
            row = Math.floor(localPosition.y + intersect.object.geometry.parameters.width / 2)
            col = Math.floor(localPosition.x + intersect.object.geometry.parameters.width / 2)
            if ((this.layerSelected === '155') || (this.layerSelected === '156') || (this.layerSelected === '157')) {

                if (intersect.object.geometry.parameters.height % 2 == 0) {
                    row = Math.floor(localPosition.y)
                    col = 1
                } else {
                    row = Math.floor(localPosition.y + 0.5)
                    col = 1
                }
            }
            let intersectL = intersects.find(element => element.object.name.includes('layer'))
            const intersectB = intersects.find(element => element.object.name.includes('box'))
            if (intersectB) intersectL = intersectB

            if (intersectL.object.name.split('_')[0] === this.layerSelected) {
                layer = this.layerSelected

                index = arrayLayers2D.findIndex(element => element.name.split('_')[0] === this.layerSelected)
                this.layerIndex = index
            }
        }

        return { row, col, layer }
    }

    dataDisplay(x, y) {
        const raycaster = this.monRayCaster(x, y)
        const intersects = raycaster.intersectObjects(this.scene.children);
        let intersect = intersects.find(element => element.object.name.includes('cardV'))
        const intersectOne = intersects.find(element => element.object.name.includes('cardV_One'))
        if (intersectOne) intersect = intersectOne
        if ((!intersect) || (!intersect.object.visible)) return
        let localPosition;
        if (intersect) {
            this.layerSelected = intersect.object.name.split('_')[0]
            const intersectIndex = intersect.object.name.split("_")[2]


            const worldPosition = intersect.point
            intersect.object.updateMatrixWorld()
            localPosition = intersect.object.worldToLocal(worldPosition)

            const row = Math.floor(localPosition.y + intersect.object.geometry.parameters.width / 2)
            const col = Math.floor(localPosition.x + intersect.object.geometry.parameters.width / 2)
            let indexDataDisplay = (row * Math.floor(intersect.object.geometry.parameters.width)) + col
            let intersectL = intersects.find(element => element.object.name.includes('layer'))
            const intersectB = intersects.find(element => element.object.name.includes('_box'))
            if (intersectB) intersectL = intersectB
            let layer = ""
            if (intersectL.object.name.split('_')[0] === this.layerSelected) {
                layer = intersectL.object
                const arrayLayers2D = this.layer.getArrayLayers2Dd()
                const index = arrayLayers2D.findIndex(element => element.name.split('_')[0] === this.layerSelected)
                this.layerIndex = index
            }

            if ((intersect != 'undefined') && (layer.visible)) {
                const maps = this.getActivationMap(this.layerSelected)
                if ((this.layerSelected == 155) || (this.layerSelected == 156) || (this.layerSelected == 157)) {

                    let indexDataDisplay = Math.floor(localPosition.y + intersect.object.geometry.parameters.height / 2)

                    const dataPixel = tf.tidy(() => { return tf.reverse2d(maps).dataSync() })
                    document.querySelector('#dataValue').innerHTML = dataPixel[indexDataDisplay].toFixed(5)

                    for (let j = 0; j < dataPixel.length; j++) {
                        for (let n = 0; n < dataPixel[j].length; n++) {
                            dataPixel[j][n].dispose()
                        }
                    }

                } else {
                    const dataPixel = tf.tidy(() => { return tf.reverse2d(maps[intersectIndex]).dataSync() })
                    const dataPixelactuel = dataPixel[indexDataDisplay]

                    if (dataPixelactuel || dataPixelactuel === 0) document.querySelector('#dataValue').innerHTML = dataPixelactuel.toFixed(5)
                    for (let j = 0; j < dataPixel.length; j++) {
                        for (let n = 0; n < dataPixel[j].length; n++) {
                            dataPixel[j][n].dispose()
                        }
                    }
                }

                for (let n = 0; n < maps.length; n++) {
                    maps[n].dispose()
                }
            } else {
                document.querySelector('#dataValue').innerHTML = ""
            }

            return indexDataDisplay
        }

    }

    _zoomClick() {
        const pos = {
            x: event.offsetX,
            y: event.offsetY + 100
        }
        this.executeMouseZoomingClick(2, event.offsetX, event.offsetY);
        this.controls.goToMousePosition(pos)

    }

    visibleOnOff() {
        this.lienVisible = true
    }

    returnTous() {
        const a2Dd = this.layer.getArrayLayers2Dd()
        const a2Dm = this.layer.getArrayLayers2Dm()
        const a3Dd = this.layer.getArrayLayers3Dd()
        const a3Dm = this.layer.getArrayLayers3Dm()
        const buttonStop = this.layer.getArrayButtonStop()
        return {
            a2Dd, a2Dm, a3Dd, a3Dm, buttonStop
        }
    }

    moreZoom() {
        this.controls.noZoom = false
        this.camera.zoom *= 1.3
        this.camera.updateProjectionMatrix()
        this.controls.noZoom = true
    }

    lessZoom() {
        this.controls.noZoom = false
        this.camera.zoom /= 1.3
        this.camera.updateProjectionMatrix()
        this.controls.noZoom = true
    }
    freeZen() {
        this.frozen = false
    }
    isMobileDevice() {
        if (navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    tick = () => {

        this.controls.update()
        this.controls.noRotate = true
        this.controls.noZoom = true
        if (this.isMobileDevice()) this.controls.noZoom = false
        TWEEN.update();
        requestAnimationFrame(this.tick)

        this.renderer.render(this.scene, this.camera)

    }
}
export default CnnScene