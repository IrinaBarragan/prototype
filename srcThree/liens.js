import { LineBasicMaterial, BufferGeometry, LineSegments } from "three"

class Liens {
  constructor(color, linewidth) {
    this.material = new LineBasicMaterial({
      color: color,
      linewidth: linewidth,

    });
    this.material.color.convertSRGBToLinear();
    this.geometry = new BufferGeometry()
    this.line = new LineSegments(this.geometry, this.material)
  }


  getLien(pointStart, pointEnd) {
    this.points = [];
    this.points.push(pointStart)
    this.points.push(pointEnd)
    const materialLine = this.material.clone()
    const geometryLine = this.geometry.clone()
    geometryLine.setFromPoints(this.points);
    this.line.geometry = geometryLine
    this.line.material = materialLine
    const line = this.line.clone()
    line.frustumCulled = false
    this.geometry.dispose()
    this.material.dispose()
    geometryLine.dispose()
    materialLine.dispose()

    return line
  }

}



export default Liens;
