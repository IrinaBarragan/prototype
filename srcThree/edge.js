import { PlaneBufferGeometry, MeshBasicMaterial, GridHelper, Group, Mesh } from 'three'
import Plane from "./plane.js";


class ImageOutline {
    constructor(w, h, color) {
        this.group = new Group()
        this.material = new MeshBasicMaterial({
            transparent:true,
            opacity: 0,
            color: "black"
          });
        this.geometry = new PlaneBufferGeometry(w+2, h+2)
        this.edge = new Mesh(this.geometry, this.material)
        this.gridHelper = new GridHelper(w, 3, color, color);
    }

    getObjet3D(i) {
        const material = this.material.clone()
        const geometry = this.geometry.clone()
        this.edge.geometry = geometry
        this.edge.material = material
        const edge = this.edge.clone()
   
        edge.position.set(0, 0, -5)
        const gridHelper = this.gridHelper.clone()
        gridHelper.rotateX(Math.PI / 2)
        const group = this.group.clone()
        group.add(edge)
        group.add(gridHelper)
        
        this.geometry.dispose()
        this.material.dispose()
        geometry.dispose()
        material.dispose()
        return group
    }


    createDataImage(shape, maps, mapIndex) {
        const plane = new Plane(maps)
        plane.createImageData(shape, maps, mapIndex)
    }
}


export default ImageOutline;
