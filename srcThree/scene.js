import { Color, Scene } from 'three';

class CreateScene {
    constructor(color) {
        this.scene = new Scene();
        this.scene.background = new Color(color);
        return this.scene
      }
  
}
export default CreateScene 
