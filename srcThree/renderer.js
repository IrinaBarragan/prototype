import { WebGLRenderer, sRGBEncoding} from 'three';

class Renderer{
    constructor(container){
  this.renderer = new WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
  this.renderer.setPixelRatio(window.devicePixelRatio*2, 2)
  this.renderer.physicallyCorrectLights = true;
  this.renderer.setSize(container.clientWidth, container.clientHeight);
  // this.renderer.gammaFactor = 2.2;
  // this.renderer.outputEncoding = sRGBEncoding;
  return this.renderer
    }
}

export { Renderer };