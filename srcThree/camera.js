
import { OrthographicCamera } from 'three'

class Camera {
  constructor(container) {
this.container = container
      let aspect = this.container.clientHeight / this.container.clientWidth
      const frustumSize = this.container.clientWidth
      let left = -frustumSize / 2
      let right = frustumSize / 2
      let bottom = -aspect * frustumSize / 2
      let top = aspect * frustumSize / 2
      let near = 100
      let far = 1000
      this.camera = new OrthographicCamera(
        left,
        right,
        top,
        bottom,
        near,
        far);

      this.camera.zoom = 1
      this.camera.updateProjectionMatrix();
      this.camera.frustumSize = 1000

    // OrthographicCamera
    return this.camera

  }
}

export default Camera