import { CircleBufferGeometry, Mesh, Sprite, MeshPhongMaterial, MeshBasicMaterial, CapsuleBufferGeometry, Vector3, SpriteMaterial, TextureLoader } from 'three'
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader.js'
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry.js'



class Circle {

  constructor(width, color) {
    this.width = width
    this.material = new MeshBasicMaterial({
      color: color
    })

    this.material.color.convertSRGBToLinear();

    this.geometry = new CircleBufferGeometry(this.width * 1.5, 70);
    this.geometryCapsule = new CapsuleBufferGeometry(this.width, this.width * 4, 4, 8);

    this.circleThreeJs = new Mesh(this.geometry, this.material);
    this.capsuleThreeJs = new Mesh(this.geometryCapsule, this.material);

    this.capsuleThreeJs.rotateZ(Math.PI / 2)
    this.txt_mat = new MeshPhongMaterial();
  }

  setPosition(x, y, z) {
    this.capsuleThreeJs.position.set(x, y, z)
  }
  getButton(txt) {
    this.buttonLess(txt)
    return this.circleThreeJs
  }


  getCapsuleThreeJs() {
    return this.capsuleThreeJs
  }


  addPredict(text, positionY = -this.width / 2, positionX = this.width) {
    text = this.checkText(text)
    const loader = new FontLoader()
    loader.load('gentilis_bold.typeface.json', (font) => {
      const textToDisplay = text.toString()

      var geometry = new TextGeometry(textToDisplay, {
        font: font,
        size: 10,
        height: 1,
        curveSegments: 10,
        bevelEnabled: false,
        bevelOffset: 0,
        bevelSegments: 1,
        bevelSize: 0.3,
        bevelThickness: 1
      });
      var txt_mesh = new Mesh(geometry, this.txt_mat);
      txt_mesh.name = "text"
      txt_mesh.castShadow = true
      txt_mesh.position.set(positionY, positionX, this.width)
      txt_mesh.rotateZ(-Math.PI / 2)

      const target = new Vector3()

      this.capsuleThreeJs.add(txt_mesh)

    });

  }

  buttonLess(text) {
    const loader = new FontLoader()

    loader.load('gentilis_bold.typeface.json', (font) => {
      if (parseInt(text) < 1) {
        text = parseInt(text * 100)

      }
      const textToDisplay = text.toString()
      var geometry = new TextGeometry(textToDisplay, {
        font: font,
        size: 25,
        height: 1,
        curveSegments: 10,
        bevelEnabled: false,
        bevelOffset: 0,
        bevelSegments: 1,
        bevelSize: 0.3,
        bevelThickness: 1
      });
      var txt_mat = new MeshPhongMaterial({ color: '#6c757d' });
      var txt_mesh = new Mesh(geometry, txt_mat);
      txt_mesh.name = "text"
      txt_mesh.castShadow = true
      // txt_mesh.position.set(-this.width/2, this.width, this.width)
      txt_mesh.position.set(-this.width / 2 - 2, -this.width, 0)

      // txt_mesh.rotateZ(-1.56)

      const target = new Vector3()

      this.circleThreeJs.add(txt_mesh)

    });

    
  }
  checkText(text){
    const name = text
		.replace('😊', ':-)')
		.replace('🙁', ':-(')
		.replace('😊', ':-(')
		.replace('🙁', ':(')
		.replace('😉', ';)')
		.replace('😉', ';-)')
		.replace('😢', ':(')
		.replace('😄', ':D')
		.replace('😜', ':P')
		.replace('❤️', '<3')
		.replace('😲', ':0')
		.replace('😄', ';D')
		.replace('😈', '^^')
		.replace('😎', 'B)')
		.replace('😟', ':S')
		.replace('😇', 'O:-(')
		.replace('😕', ':-')
	
			return name

  }

}

export default Circle