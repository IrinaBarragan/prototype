import PlaneObject from './planeObject.js'
import CnnScene from './modelScene.js'
import CubeAndSphere from './cubeAndSphere.js'
import Plane from './plane.js'
import Circle from './circle.js'
import ModelInfo from './modelInfoStory.js'
import { Vector2, Raycaster } from 'three'
import TWEEN from '@tweenjs/tween.js'



class CreateLayerOfModel {
    constructor(container, activationCards, scene, camera, labels) {
        this.activationCards = activationCards

        this.container = container
        this.scene = scene
        this.camera = camera
        // 3Dm: ThreeD miminify
        // 3Dd: ThreeD deploy
        this.arrayLayers3Dm = []
        this.arrayLayers3Dd = []
        this.arrayLayers2Dd = []
        this.arrayLayers2Dm = []
        this.arrayButtonStop = []
        this.arrayInfo = []

        this.lengthUsed = 0
        this.raycaster = new Raycaster();
        this.layerIndex
        this.width = 0;
        this.layerBox3Dm
        this.buttonStop
        this.objs = []
        this.labels = labels
        // this.modelScene = new CnnScene()


    }


    init(layer, bgColor, y, axis) {
        this.y = y
        const maps = this.getActivationMap(layer)
        const shape = maps[0].shape[1]
        const numberOfCards = maps.length
        let widthContainer = shape + numberOfCards * 0.1 + 30
        this.width = this.setMinimumWidthLayerContainer(widthContainer)

        this.lengthUsed += this.width / 2
        const start = - this.container.clientWidth / 2 + 10
        this.startLayer = start + this.lengthUsed

        this.layerBox = this.createLayerContainer(layer, this.width, bgColor)
        this.layer3DStateMinimized(layer, shape, numberOfCards, this.startLayer)
        this.layer2DStateDeployed(layer, maps, shape, shape, numberOfCards, axis)
        const heightContainer = (shape + 3) * numberOfCards

        // button
        this.buttonStop = this.createButtonStop('-')
        this.buttonStop.position.set(0 - 2.5, heightContainer / 2 + 20, 100)
        this.buttonStop.name = `${layer}_buttonStop`
        this.buttonStop.visible = false
        this.layerBox.add(this.buttonStop)
        this.arrayButtonStop.push(this.buttonStop)

        for (let n = 0; n < maps.length; n++) {
            maps[n].dispose()
        }
        return this.layerBox
    }

    getWidthOfLayer() {
        return this.width
    }


    initShapeEqualOne(layer, bgColor, y, axis) {
        // 152, 155, 157
        this.y = y
        const maps = this.getActivationMap(layer)
        const shape = 1
        const zoom = 7
        const numberOfCards = maps.shape[1]


        let widthContainer = shape + numberOfCards * 0.1 + 20
        this.width = this.setMinimumWidthLayerContainer(widthContainer) + 10
        const start = - this.container.clientWidth / 2 + 10
        this.lengthUsed += this.width / 2
        this.startLayer = start + this.lengthUsed
        this.layerBox = this.createLayerContainer(layer, this.width, bgColor)
        const heightContainer = shape * (numberOfCards + 3)

        // button
        if ((layer == 155) || (layer == 156)) {
            this.buttonStop = this.createButtonStop()
            this.buttonStop.position.set(0, heightContainer / 2 + 20, 300)
            this.buttonStop.name = `${layer}_buttonStop`
            this.buttonStop.visible = false
            this.layerBox.add(this.buttonStop)
            this.arrayButtonStop.push(this.buttonStop)
        }
        this.layer3DStateMinimizedForLayersShapeOne(layer, shape, numberOfCards, zoom)
        this.layer2DStateDeployedForLayresShapeEqualOne(layer, maps, shape, shape, numberOfCards, zoom)
        // maps.dispose()

        return this.layerBox
    }
    getActivationMap(layer) {
        return tf.tidy(() => {
            if ((layer == 155) || (layer == 156) || (layer == 157)) {
                return this.activationCards[layer]
            } else {
                return tf.tidy(() => {
                    return tf.unstack(this.activationCards[layer].reshape([this.activationCards[layer].shape[1], this.activationCards[layer].shape[2], this.activationCards[layer].shape[3]]), 2)

                });

            }

        })
    }
    getLengthUsed() {
        return this.lengthUsed
    }

    clearTensors() {
        for (let n = 0; n < this.activationCards.length; n++) {
            this.activationCards[n].dispose()
        }

    }
    initBox(image, y) {
        console.log(image)
        this.y = y


        this.width = this.setMinimumWidthLayerContainer(224 + 30)

        this.lengthUsed = 0
        const start = - this.container.clientWidth / 2 + 10
        this.startLayer = start + this.lengthUsed
        this.width = 224


        const box = new PlaneObject(this.width, this.width)
        box.imageTexture(image.src)
        this.layerBox = this.createLayerContainer(0, this.width, "white")
        this.layerBox.position.set(this.startLayer, 0, 0)
        this.box = box.getObjet3D()
        this.box.position.set(0, 0, 10)
        this.layerBox.add(this.box)
          let depth = 0.2
        let radius = 7
        let smoothness = 5
        
        let box2= new PlaneObject(45, 45)
        let box3= new PlaneObject(55, 55, "#9e9e9e")
        box2.imageTexture('icon-camera_forTJ.png')
        const buttonOpenContainerToChangeInputImage = box2.getObjet3D()
        const backgroundButton = box3.getObjet3D()

        backgroundButton.geometry= box3.createBoxWithRoundedEdges(50, 50, depth, radius, smoothness)
        backgroundButton.position.set(0, this.width, 20)
        buttonOpenContainerToChangeInputImage.position.set(0, 0, 1)
        backgroundButton.name = 'buttonCameraOpen'
        backgroundButton.add(buttonOpenContainerToChangeInputImage)
        this.layerBox.add(backgroundButton)


        this.lengthUsed += this.width / 2 + 20
        this.startLayer = this.startLayer + this.lengthUsed
        return this.layerBox
    }
    createLayerContainer(layer, width, bgColor) {
        const maxHeight = this.getMaxHeigth()
        const box = new PlaneObject(width, maxHeight, bgColor, this.container)
        this.layerBox3Dm = box.getObjet3D()
        this.layerBox3Dm.position.set(this.startLayer, 0, 10)
        this.layerBox3Dm.name = `${layer}__box`
        this.lengthUsed += width / 2
        this.startLayer = this.startLayer + this.lengthUsed
        return this.layerBox3Dm
    }
    setMinimumWidthLayerContainer(widthContainer) {
        const widthMin = 50
        if (widthContainer < widthMin) widthContainer = widthMin
        return widthContainer
    }
    layer3DStateMinimized(layer, shape, numberOfCards) {
        const cube = new CubeAndSphere(shape, numberOfCards)
        const cubeThreeJs = cube.getObjet()
        cubeThreeJs.name = `${layer}_layer`
        this.arrayLayers3Dm.push(cubeThreeJs)
        cubeThreeJs.position.set(0, 0, 200)
        this.layerBox.add(cubeThreeJs)
    }

    addPanelInfo(text, fontSize, nombreLayers) {
        const infoText = new ModelInfo(this.width, 100)
        const modelInfo = infoText.getObjet()
        infoText.addText(text, this.y, fontSize)
        // in order to have line space we reduce y
        this.y -= 16
        return modelInfo
    }

    layer3DStateMinimizedForLayersShapeOne(layer, shape, numberOfCards, zoom) {

        const cube = new CubeAndSphere(shape, numberOfCards, zoom)
        const sphereThreeJs = cube.getSphere()
        sphereThreeJs.name = `${layer}_layer`
        this.arrayLayers3Dm.push(sphereThreeJs)
        sphereThreeJs.position.set(0, 0, 200)
        this.layerBox.add(sphereThreeJs)
    }
    getIndex() {
        if (this.layerIndex > 1) {
            return false
        } else {
            return true
        }
    }

    layer2DStateDeployed(layer, maps, shape, shapeDataImage, numberOfCards, axis) {
        const plane = new Plane(maps)
        let res = plane.init(shape, shapeDataImage, numberOfCards, layer, 0, 0, 200, axis)
        plane.matrixAutoUpdate = false
        this.groupOfPlanes2Dm = res.box2
        // this.groupOfPlanes2Dm.name = `${layer}_layer`
        this.arrayLayers2Dm.push(this.groupOfPlanes2Dm)
        this.layerBox.add(this.groupOfPlanes2Dm)
        this.groupOfPlanes2Dm.visible = false

        this.groupOfPlanes2D = res.box1
        this.groupOfPlanes2D.name = `${layer}_layer`
        this.arrayLayers2Dd.push(this.groupOfPlanes2D)
        this.layerBox.add(this.groupOfPlanes2D)
        this.groupOfPlanes2D.visible = false
        for (let n = 0; n < this.activationCards[layer].length; n++) {
            this.activationCards[layer][n].dispose()
        }
        for (let n = 0; n < res.length; n++) {
            res[n].dispose()
        }
        for (let n = 0; n < maps.length; n++) {
            maps[n].dispose()
        }

    }

    layer2DStateDeployedForLayresShapeEqualOne(layer, maps, shape, shapeDataImage, numberOfCards, zoom, axis) {
        this.groupOfPlanes2Dm = new CubeAndSphere(shape * zoom, numberOfCards, zoom).getObjet()
        this.groupOfPlanes2Dm.rotation.set(0, Math.PI / 2, 0);
        this.groupOfPlanes2Dm.visible = false
        this.groupOfPlanes2Dm.name = `${layer}__layer`
        this.arrayLayers2Dm.push(this.groupOfPlanes2Dm)
        this.layerBox.add(this.groupOfPlanes2Dm)

        const plane = new Plane(maps)
        let a = plane.initLayersShapeOne(shape * 5, shapeDataImage, layer, 0, 0, 100, axis)
        plane.matrixAutoUpdate = false
        this.groupOfPlanes2D = a.box1
        // this.groupOfPlanes2D.name = `${layer}__layer`
        this.arrayLayers2Dd.push(this.groupOfPlanes2D)
        this.layerBox.add(this.groupOfPlanes2D)
        this.groupOfPlanes2D.visible = false

        if (layer === 157) {
            this.predictResult(maps)
        }
        for (let n = 0; n < this.activationCards[layer].length; n++) {
            this.activationCards[layer][n].dispose()
        }
        for (let n = 0; n < maps.length; n++) {
            maps[n].dispose()
        }
    }
    predictResult(maps) {
        let metadataFromStorage = localStorage.getItem('modelMetadata')
        metadataFromStorage = JSON.parse(metadataFromStorage);
        let labelsMetaData = this.labels
        if (metadataFromStorage) labelsMetaData = metadataFromStorage.labels
   
        const numberOfCategory = maps.dataSync().length
        const width = 10
        let start = ((width + 5) * numberOfCategory * 4) / 2 - 10

        for (let i = 0; i < numberOfCategory; i++) {
            const circleForTitle = new Circle(width, '#22b57300', this.scene)
            circleForTitle.txt_mat.color.set("black")
            const title = circleForTitle.getCapsuleThreeJs()
            this.layerBox.add(title)
            circleForTitle.setPosition(40, start, 0)
            let titleText
            if (labelsMetaData) {
                titleText = labelsMetaData[i]
            } else {

                titleText = "Category" + (i + 1).toString()
            }
            const textTitle = circleForTitle.addPredict(titleText)
            start = start - (width + 5) * 2
            title.visible = true

            const circle = new Circle(width, '#22b573', this.scene)
            circle.txt_mat.color.set("white")
            const result = circle.getCapsuleThreeJs()
            this.layerBox.add(result)

            circle.setPosition(60, start, 0)
            let text = Math.floor((maps.dataSync()[i]+0.005) * 100)
            text = text.toString() + '%'
            circle.addPredict(text)

            start = start - (width + 5) * 2
            this.layerBox.add(result)
            result.visible = true


            // this.arrayLayers2Dd.push(result)
        }
        for (let n = 0; n < maps.length; n++) {
            maps.dispose()
        }

    }
    getMaxHeigth() {
        const maps = this.getActivationMap(153)
        const maxHeight = maps[0].shape[0] * maps.length - this.container.clientHeight / 2
        for (let n = 0; n < maps.length; n++) {
            maps[n].dispose()
        }
        return maxHeight
    }

    createButtonStop(txt = '-') {
        const width = 10;
        const color = '#f8f9fa'
        const cercle = new Circle(width, color)
        const button = cercle.getButton(txt)

        return button
    }

    click() {
        // transition 3D to 2D
        if ((this.arrayLayers3Dm[0].visible === true)) {

            this.arrayLayers2Dd.forEach(element => {
                setTimeout(function () {
                    element.visible = true
                }, 1000)
            });
            this.arrayLayers2Dm.forEach(element => {
                setTimeout(function () {
                    element.visible = false
                }, 1000)
            });
            this.arrayLayers3Dm.forEach(element => {
                new TWEEN.Tween(element.rotation)
                    .to({ x: 0, y: Math.PI / 4, z: 0 }, 1000)
                    .easing(TWEEN.Easing.Bounce.In)
                    .start()

                setTimeout(function () {
                    element.visible = false
                }, 1000)

            });
            TWEEN.update();
        }
        this.scene.traverse((object) => {
            if (object.isMesh) {
                this.objs.push(object)
            }
        })
        this.monRayCaster()

        const intersects = this.raycaster.intersectObjects(this.scene.children);
        let closeInfo = intersects.find(element => element.object.name.includes('closeComposantInfo'))
        const info = intersects.find(element => element.object.name.includes('information'))
        const button = intersects.find(element => element.object.name.includes('buttonStop'))
        const boxInfoBackground = this.objs.find(element => element.name === "boxInfoBackground")
        const outlineMesh = this.objs.find(element => element.name === "outlineMesh")

        if ((info) && (info.object.visible)) {
            info.object.visible = false
            this.arrayInfo.forEach(el => el.visible = true)
            boxInfoBackground.visible = true
            outlineMesh.visible = true
            closeInfo = this.objs.find(element => element.name.includes('closeComposantInfo'))
            closeInfo.visible = true
        }
        else if ((closeInfo) && (closeInfo.object.visible)) {
            this.arrayInfo.forEach(el => el.visible = false)
            boxInfoBackground.visible = false
            outlineMesh.visible = false
            closeInfo.object.visible = false

            try {
                const info = this.objs.find(element => element.name.includes('information'))
                info.visible = true
            } catch {

            }
        }
        else if (button) {
            this.closeLayer(button)

        } else {
            const layer = intersects.find(element => element.object.name.includes('layer'))
            const box = intersects.find(element => element.object.name.includes('box'))
            if (layer) this.layerSelected = layer.object.name.split('_')[0]
            if (box) this.layerSelected = box.object.name.split('_')[0]
            const index = this.arrayLayers2Dd.findIndex(element => element.name.split('_')[0] === this.layerSelected)
            this.layerIndex = index
            const modeCardsDeployed = this.arrayLayers2Dd[index]
            const modeCardsMinimized = this.arrayLayers2Dm[index]
            const buttonStop = this.arrayButtonStop[index]

            setTimeout(function () {
                if (buttonStop) buttonStop.visible = true
                if (modeCardsDeployed) {
                    modeCardsDeployed.visible = true
                    modeCardsDeployed.children.forEach(el => el.visible = true)
                }
                if (modeCardsMinimized) modeCardsMinimized.children.forEach(el => el.visible = false)
            }, 1000)
        }
    }

    closeLayer(button) {

        this.layerSelected = button.object.name.split("_")[0]
        const index = this.arrayLayers2Dd.findIndex(element => element.name.split("_")[0] === this.layerSelected)
        const modeCardsDeployed = this.arrayLayers2Dd[index]
        const modeCardsMinimized = this.arrayLayers2Dm[index]
        const buttonStop = this.arrayButtonStop[index]
        setTimeout(function () {
            buttonStop.visible = false
            modeCardsDeployed.visible = false
            modeCardsDeployed.children.forEach(el => el.visible = false)

            modeCardsMinimized.visible = true
            modeCardsMinimized.children.forEach(el => el.visible = true)
        }, 200)

    }

    _zoomClick(controlsZoom, position, camera, container) {
        this.monRayCaster()
        const intersects = this.raycaster.intersectObjects(this.scene.children);
        const intersect = intersects.find(element => element.object.name.includes('cardV'))
        let width
        if (intersect.object.name.includes('cardV_One')) { width = 7 }
        else width = intersect.object.geometry.parameters.width
        controlsZoom.zoomToCenter(position, camera, container, width);
        this.camera.updateProjectionMatrix();
    }

    monRayCaster() {
        const mouse = new Vector2()
        mouse.x = event.offsetX / this.container.clientWidth * 2 - 1
        mouse.y = -(event.offsetY / this.container.clientHeight) * 2 + 1
        this.raycaster.ray.origin.set(mouse.x, mouse.y, 800);
        this.camera.localToWorld(this.raycaster.ray.origin);
        this.raycaster.ray.origin.z = this.camera.far;
        this.raycaster.setFromCamera(mouse, this.camera);
    }


    getLayerAndIndex() {
        const layer = this.layerSelected
        const index = this.layerIndex
        return { layer, index }
    }
    getArrayLayers3Dm() {
        return this.arrayLayers3Dm
    }
    getArrayLayers3Dd() {
        return this.arrayLayers3Dd
    }
    getArrayLayers2Dd() {
        return this.arrayLayers2Dd
    }
    getArrayLayers2Dm() {
        return this.arrayLayers2Dm
    }
    getArrayButtonStop() {
        return this.arrayButtonStop
    }

}

export default CreateLayerOfModel
