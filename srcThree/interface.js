import Controller from '../src/controller.js'
import CnnScene from './modelScene.js'
import { getActivCardsAndWeights } from '../src/getActivationMap.js'
import * as modifTHML from '../src/modifElementHTML.js'
import { DefaultLoadingManager } from 'three'


class Interface {

    // 1. Create an instance of the World app
    constructor(container, model, image) {

        this.model = model
        this.container = container
        this.boxVideoImage = false
        this.infoBlockVisible = false
        this.palette = "cooltowarm"
        const flip = this.flipImage(image)
        const sceneCreated = this._createScene3D(image, image, true)

        if (sceneCreated) this.controller = new Controller()

        this.webCameraOn = false
        this.webcam = new tmImage.Webcam(200, 200, true) // width, height, flip


    }

    async _createScene3D(image, imgFlip, flip, axis) {

        const result = await this.initMaps(image, flip)
        this.activationCards = result.activationCards
        const weights = result.layersWeights

        document.querySelector("#videoImage").classList = 'btn btn-primary'
        this.screenShot = document.querySelector("#realTemps")
        this.screenShot.style.display = 'none'
        if (this.infoBlockVisible === true) {
            document.querySelector('#infoBlock').classList = 'btn btn-primary'
        } else {
            document.querySelector('#infoBlock').classList = 'btn btn-outline-secondary'
        }
        if (this.webCameraOn) {
            document.getElementById("image-container").style.display = 'none'
            this.streamingCamera()
        }

        // stop spinner when three.js scene is loaded
        DefaultLoadingManager.onLoad = function () {
            document.querySelector(".loader").style.display = 'none'
        };
        if (!imgFlip) imgFlip = image
        const bias = this.model.model.layers[1].layers[0].bias.val.dataSync()
        const prediction = await this.displayPrediction(image)

        this.myScene3D = new CnnScene(this.container, this.activationCards, image, imgFlip, weights, this.model.getClassLabels(), bias, prediction, axis)
        const modelLayersName = []
        this.model.model.layers[0].layers[0].layers.forEach(el=>modelLayersName.push(el.name))
        this.myScene3D.init(modelLayersName)
        this.myScene3D.render()
        return true

    }

    async displayPrediction(image) {
        const prediction = await this.model.predict(image)
        for (let i = 0; i < this.model.getTotalClasses(); i++) {
            const classPrediction =
                prediction[i].className + ': ' + prediction[i].probability.toFixed(2)
            document.querySelector('#prediction').childNodes[i].innerHTML = classPrediction
        }
        return prediction

    }

    async init() {
        // buttons header
        this.controller.on("click2D", "click2D", () => {
            this._switchCardActivationDisplay()
        })
        this.controller.on("click3D", "click3D", () => {
            this._click3D()
        })

        // boxImageVideo
        this.controller.on('closeModuleInput', 'closeModuleInput', () => {
            this.closeModuleInput()
        })
        this.controller.on('openModuleInput', 'openModuleInput', () => {
            this.openModuleInput()
        })
        this.controller.on('toggleFichierCamera', 'toggleFichierCamera', () => {
            this._toggleFichierCamera()
        })
        this.controller.on('closeModal', 'closeModal', () => {
            this.closeModal()
        })
        this.controller.on('uploadImage', 'uploadImage', () => {
            this._uploadImage()
        })
        this.controller.on("cameraSource", "cameraSource", () => {
            this.setCameraSource()
        })


        // zoomEvents
        this.controller.on('modalDisplay', 'modalDisplay', () => {
            this.displayModal()
        })
        this.controller.on("dispayInfoBlock", "displayInfoBlock", () => {
            (this.infoBlockVisible) ? this.infoBlockVisible = false : this.infoBlockVisible = true
            this.displayInfoBlock()
        })
        this.controller.on("moreZoom", "moreZoom", () => {
            this._moreZoom()
        })
        this.controller.on("lessZoom", "lessZoom", () => {
            this._lessZoom()
        })
        this.controller.on("screenInitial", "screenInitial", () => {
            this._screenInitial()
        })


        // palette
        this.controller.on("changePalette", "changePalette", () => {
            this._changePalette()
        })

    }


    // boxImageVideo
    closeModuleInput() {
        document.querySelector("#imageBox").style.display = 'none'
        this.boxVideoImage = false
        document.querySelector("#videoImage").classList = 'btn btn-outline-secondary'
        if(this.myScene3D ) this.myScene3D.buttonCameraOpen.object.visible = true

    }
    openModuleInput() {
        this.boxVideoImage === true ? this.boxVideoImage = false : this.boxVideoImage = true

        if (this.boxVideoImage) {
            document.querySelector("#imageBox").style.display = 'block'
            document.querySelector('#realTemps').style.display = 'block'
            document.querySelector("#videoImage").classList = 'btn btn-primary'

        } else {
            document.querySelector("#imageBox").style.display = 'none'
            document.querySelector('#realTemps').style.display = 'none'
            document.querySelector("#videoImage").classList = 'btn btn-outline-secondary'
   

        }

    }
    displayModal() {
        document.getElementById("modalUploadImage").style.display = "block"
        document.getElementsByClassName("modal-content")[0].style.display = "block"


    }
    closeModal() {
        document.getElementsByClassName("modal-content")[0].style.display = "none"
    }


    async streamingCamera() {
        await this.webcam.setup() // request access to the webcam
        await this.webcam.play()
        const webCanvas = document.getElementById('webcam-container').appendChild(this.webcam.canvas)
        webCanvas.setAttribute("id", "webCanvas")
        requestAnimationFrame(this.loop)


    }
    async setCameraSource() {
        const img = await (await this.getScreenshot(document.querySelector("#webCanvas"))).img
        const imgFlip = await (await this.getScreenshot(document.querySelector("#webCanvas"))).imgFlip
        await this.displayPrediction(img)
        await this.webcam.pause()
        for (let n = 0; n < this.activationCards.length; n++) {
            this.activationCards[n].dispose()
        }
        document.querySelector("#canvasThreeJs").remove()
        document.querySelector(".loader").style.display = "block"

        await this.changeActivationCards(img, imgFlip, false)
        await this.webcam.play()
        this.closeModuleInput()
        requestAnimationFrame(this.loop)

    }

    async getScreenshot(videoEl) {
        const canvas = document.createElement('canvas')
        canvas.width = videoEl.clientWidth
        canvas.height = videoEl.clientHeight
        canvas.getContext('2d').scale(-1, 1);
        canvas.getContext('2d').drawImage(videoEl, 0, 0, canvas.width * -1, canvas.height)
        const img = new Image(canvas.width, canvas.height)
        img.src = canvas.toDataURL()


        const canvas1 = document.createElement('canvas')
        canvas1.width = videoEl.clientWidth
        canvas1.height = videoEl.clientHeight
        canvas1.getContext('2d').drawImage(videoEl, 0, 0, canvas1.width, canvas1.height)
        const imgFlip = new Image(canvas1.width, canvas1.height)
        imgFlip.src = canvas1.toDataURL()
        const screenShots = { img, imgFlip }
        return screenShots
    }


    _toggleFichierCamera() {
        this.webCameraOn === true ? this.webCameraOn = false : this.webCameraOn = true
        if (this.webCameraOn) {
            document.getElementById("image-container").style.display = 'none'
            document.getElementById("webcam-container").style.display = "block"
            this.screenShot.style.display = 'block'

            this.streamingCamera()
        } else {
            document.getElementById("image-container").style.display = 'block'
            document.getElementById("webcam-container").style.display = "none"
            this.screenShot.style.display = 'none'
        }
    }
    flipImage(el) {
        const canvas1 = document.createElement('canvas')
        canvas1.width = el.clientWidth
        canvas1.height = el.clientHeight
        canvas1.getContext('2d').drawImage(el, 0, 0, canvas1.width, canvas1.height)
        const imgFlip = new Image(canvas1.width, canvas1.height)
        imgFlip.src = canvas1.toDataURL()
        return img
    }

    async _uploadImage() {
        const preview = document.querySelector('#image-input');
        const file = document.querySelector('input[type=file]').files[0];
        const reader = new FileReader();

        reader.addEventListener("load", async () => {
            const newImage = new Image()
            newImage.src = reader.result
            await this.changeActivationCards(newImage, newImage, false, 0)
        }, false);

        if (file) {
            reader.readAsDataURL(file);
            document.getElementById("modalUploadImage").style.display = "none"
        }
        this.myScene3D.remove()

        // document.querySelector(".loader").style.display = 'block'
        for (let n = 0; n < this.activationCards.length; n++) {
            this.activationCards[n].dispose()
        }

        document.querySelector("#canvasThreeJs").remove()
        document.querySelector(".loader").style.display = "block"

        this.closeModuleInput()

    }

    displayInfoBlock() {
        this.myScene3D.displayInfoBlock(this.infoBlockVisible)

        if (this.infoBlockVisible === true) {
            document.querySelector('#infoBlock').classList = 'btn btn-primary'
        } else {
            document.querySelector('#infoBlock').classList = 'btn btn-outline-secondary'
        }
    }

    async changeActivationCards(image, imgFlip, flip, axis) {
        let simple = this.activationCards[5][8]
        for (let i = 0; i < this.activationCards.length; i++) {
            this.activationCards[i].dispose()
        }
        const result = await this.initMaps(image, flip)
        this.activationCards = result.activationCards
        if (this.activationCards[5][8] === simple) {
            for (let i = 0; i < this.activationCards.length; i++) {
                this.activationCards[i].dispose()
            }
            const result = await this.initMaps(image, flip)
            this.activationCards = result.activationCards
        }
        if (this.activationCards[5][8] === simple) {
            for (let i = 0; i < this.activationCards.length; i++) {
                this.activationCards[i].dispose()
            }
            const result = await this.initMaps(image, flip)
            this.activationCards = result.activationCards
        }
        simple = this.activationCards[5][8]
        this._createScene3D(image, imgFlip, false, axis)

    }

    async initMaps(image, flip) {
        const result = await getActivCardsAndWeights(image, this.model, flip)
        return result
    }

    // zoom 
    _moreZoom() {
        this.myScene3D.moreZoom()
    }
    _screenInitial() {
        this.myScene3D.setCameraToInitialParameters()

    }
    _lessZoom() {
        this.myScene3D.lessZoom()
    }
    // palette
    _changePalette() {
        const palette = document.querySelector('select')
        const elts = palette.querySelectorAll('option')
        let i = 0
        for (i = 0; i < elts.length; i++) {
            if (elts[i].selected === true)
                break
        }
        this.palette = elts[i].value

    }

    // buttons header
    _switchCardActivationDisplay() {
        modifTHML.switchButton3D2D(true, false)
        this.myScene3D.displayCardsActivation(true, true, false, false)
    }
    _click3D() {
        modifTHML.switchButton3D2D(false, true)
        this.myScene3D.displayCardsActivation(false, false, true, false)
        this.myScene3D.lienVisible = false
        this.myScene3D.linkInvisible()
    }


    loop = () => {
        this.webcam.update()
        requestAnimationFrame(this.loop)

    }


}
export default Interface 