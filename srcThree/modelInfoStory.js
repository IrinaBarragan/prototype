import { BoxBufferGeometry, Mesh, MeshBasicMaterial, MeshPhongMaterial, Vector3 } from 'three';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader.js'
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry.js'




class ModelInfo {
  constructor(width, height) {

    this.geometry = new BoxBufferGeometry(width, height, 0.2);
    this.material = new MeshBasicMaterial({
      transparent: true, opacity: 0.0
    });
    this.mesh = new Mesh(this.geometry, this.material);

  }

  getObjet() {
    return this.mesh
  }

  addText(text, y, fontSize) {
    const loader = new FontLoader()
    loader.load('gentilis_bold.typeface.json', (font) => {
      const textToDisplay = text.toString()
      const geometry = new TextGeometry(textToDisplay, {
        font: font,
        size: fontSize,
        height: 1,
        curveSegments: 1,
        bevelEnabled: false,
        bevelOffset: 0,
        bevelSegments: 1,
        bevelSize: 0.3,
        bevelThickness: 1
      });

      const txt_mat = new MeshPhongMaterial({ color: 'black' });
      const txt_mesh = new Mesh(geometry, txt_mat);
      txt_mesh.name = "text"
      txt_mesh.castShadow = true

            // find centre
            geometry.computeBoundingBox();
            const center = geometry.boundingBox.getCenter(new Vector3());
            txt_mesh.position.set(-center.x, y, 450)

      this.mesh.add(txt_mesh)

    });

  }
}

export default ModelInfo;