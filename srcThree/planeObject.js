import { sRGBEncoding, PlaneBufferGeometry, Mesh, MeshBasicMaterial, Shape, ExtrudeBufferGeometry, TextureLoader } from 'three'

class PlaneObject {
  constructor(width, height, color) {

    this.geometry = new PlaneBufferGeometry(width, height);
    this.material = new MeshBasicMaterial({
      color: color
    });
  
    // this.material.color.convertSRGBToLinear();

    // create a Mesh containing the geometry and material
    this.plane = new Mesh(this.geometry, this.material);
    this.plane.position.set(0, 0, 0)
    this.plane.renderOrder = 1;

  }

  getObjet3D() {
    const material = this.material.clone()
    const geometry = this.geometry.clone()
    const plane = this.plane.clone()
    plane.material = material
    plane.geometry = geometry
    this.geometry.dispose()
    geometry.dispose()
    this.material.dispose()
    material.dispose()
    return plane
  }

  imageTexture(img) {
    const texture = new TextureLoader().load(img);
    // texture.encoding = sRGBEncoding;
    this.material.map = texture

  }

  createBoxWithRoundedEdges(width, height, depth, radius0, smoothness) {
    let shape = new Shape();
    let eps = 0.00001;
    let radius = radius0 - eps;
    shape.absarc(eps, eps, eps, -Math.PI / 2, -Math.PI, true);
    shape.absarc(eps, height - radius * 2, eps, Math.PI, Math.PI / 2, true);
    shape.absarc(width - radius * 2, height - radius * 2, eps, Math.PI / 2, 0, true);
    shape.absarc(width - radius * 2, eps, eps, 0, -Math.PI / 2, true);
    let geometry = new ExtrudeBufferGeometry(shape, {
      depth: depth - radius0 * 2,
      bevelEnabled: true,
      bevelSegments: smoothness * 2,
      steps: 1,
      bevelSize: radius,
      bevelThickness: radius0,
      curveSegments: smoothness
    });

    geometry.center();

    return geometry;
  }
}

export default PlaneObject;